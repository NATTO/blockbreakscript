using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
/// <summary>
/// BGMとSEを管理するクラス
/// </summary>
[RequireComponent(typeof(AudioSource))]
[RequireComponent(typeof(AudioSource))]
public class SoundManager : SingletonMonoBehaviour<SoundManager>
{
    const string SE_DIR = "SE/";
    const string BGM_DIR = "BGM/";
    const float DEFAULT_FADEOUT_TIME = 1f;

    AudioSource bgmAudioSource;
    AudioSource seAudioSource;

    Dictionary<BGM_ID, AudioClip> bgmClips = new Dictionary<BGM_ID, AudioClip>();
    Dictionary<SE_ID, AudioClip> seClips = new Dictionary<SE_ID, AudioClip>();

    protected override void Awake()
    {
        base.Awake();
        Initialize();
    }

    void Initialize()
    {
        var audios = GetComponents<AudioSource>();
        bgmAudioSource = audios[0];
        seAudioSource = audios[1];
        int lastBGM = Enum.GetValues(typeof(BGM_ID)).Cast<int>().Max();
        int lastSE = Enum.GetValues(typeof(SE_ID)).Cast<int>().Max();
        var allBGM = ObjectLoad<AudioClip>.ResourcesLoadAll(BGM_DIR);
        var allSE = ObjectLoad<AudioClip>.ResourcesLoadAll(SE_DIR);

        for (int bgmNum = 0; bgmNum <= lastBGM; bgmNum++)
        {
            var bgmObject = allBGM.First(bgm => ((BGM_ID)bgmNum).ToString() == bgm.name);
            if (bgmObject != null)
                bgmClips.Add((BGM_ID)bgmNum, bgmObject);
        }
        for (int seNum = 0; seNum <= lastSE; seNum++)
        {
            var seObject = allSE.FirstOrDefault(se => ((SE_ID)seNum).ToString() == se.name);
            if (seObject != null)
                seClips.Add((SE_ID)seNum, seObject);
        }
    }

    public void SEPlay(AudioClip clip)
    {
        bgmAudioSource.PlayOneShot(clip);
    }

    public void SEPlay(SE_ID id)
    {
        if (seClips.ContainsKey(id))
        {
            seAudioSource.PlayOneShot(seClips[id]);
        }
        else
        {
            Debug.LogError($"{id}が存在しません。{SE_DIR}を確認してみてください");
        }
    }

    public void BGMPlay(BGM_ID id)
    {
        if (bgmClips.ContainsKey(id))
        {
            bgmAudioSource.clip = bgmClips[id];
            bgmAudioSource.Play();
        }
        else
        {
            Debug.LogError($"{id}が存在しません。{BGM_DIR}を確認してみてください");
        }
    }

    public void BGMFadeOut(float time = DEFAULT_FADEOUT_TIME)
    {
        StartCoroutine(BGMFadeOutCoroutine(time));
    }

    public void BGMReStart()
    {
        bgmAudioSource.Play();
    }

    IEnumerator BGMFadeOutCoroutine(float time)
    {
        float progress = 1;
        float t = 0;
        while (progress > 0)
        {
            bgmAudioSource.volume = progress;
            progress = Mathf.Lerp(1, 0, t);
            t += Time.deltaTime / time;
            yield return null;
        }
        bgmAudioSource.Stop();
        bgmAudioSource.volume = 1;
    }
}

static class ObjectLoad<T> where T : UnityEngine.Object
{
    public static T ResourcesLoad(string path)
    {
        return Resources.Load<T>(path);
    }
    public static T[] ResourcesLoadAll(string path)
    {
        return Resources.LoadAll<T>(path);
    }
}

public enum BGM_ID
{
    TITLE = 0,
    GAME_IN,
    TUTORIAL,
}

public enum SE_ID
{
    BUTTON_CLICK = 0,
    BLOCK_DESTROY,
    BLOCK_STRIKE,
    BLOCK_MOVE,
    BLOCK_FALL,
    BLOCK_FALLEND,
    GAME_START,
    PLAYER_DEATH,
    RANKING_OPEN,
    HIGH_SCORE,
    STAGE_CLEAR
}