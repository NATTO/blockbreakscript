using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// BGMを再生させるクラス
/// </summary>
public class BGMPlay : MonoBehaviour
{
    [SerializeField]
    BGM_ID id;

    private void Start()
    {
        SoundManager.Instance.BGMPlay(id);
    }
}
