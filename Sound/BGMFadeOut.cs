using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// ボタンにつけてBGMを止めるクラス
/// </summary>
public class BGMFadeOut : MonoBehaviour
{
    public void BGMFadeout(float time = -1)
    {
        if(time < 0)
        {
            SoundManager.Instance.BGMFadeOut();
        }
        else
        {
            SoundManager.Instance.BGMFadeOut(time);
        }
    }
}
