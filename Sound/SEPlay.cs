using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SEPlay : MonoBehaviour
{
    public void Play(int id)
    {
        SoundManager.Instance.SEPlay((SE_ID)id);
    }
}
