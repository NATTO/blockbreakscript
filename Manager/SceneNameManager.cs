using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// シーンの名前クラス
/// </summary>
public static class SceneNameManager
{
    public const string BlockGameScene = "BlockGameScene";
    public const string AwakeScene = "AwakeScene";
    public const string DontDestroyOnLoad = "DontDestroyOnLoad";
    public const string TitleScene = "TitleScene";
    public const string TutorialScene = "TutorialScene";
}
