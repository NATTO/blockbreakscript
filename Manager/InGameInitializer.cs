using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// ゲーム初期化用のクラス
/// </summary>
[RequireComponent(typeof(BlockGenerator))]
[RequireComponent(typeof(BlockManager))]
[RequireComponent(typeof(GameProgress))]
[RequireComponent(typeof(GameScore))]
public class InGameInitializer : MonoBehaviour
{
    [SerializeField]
    BlockSpriteSettings spriteSettings;
    [SerializeField]
    HighScoreProgress highScoreManager;

#if UNITY_EDITOR 
    [SerializeField]
    bool isDebug;
    [SerializeField]
    BlockDataSettings debugDataSettings;
    [SerializeField]
    ProgressSettings debugProgressSettings;
#endif

    [SerializeField]
    BlockGenerator blockGenerator;
    [SerializeField]
    BlockManager blockManager;
    [SerializeField]
    GameScore gameScore;
    [SerializeField]
    GameProgress gameProgress;
    [SerializeField]
    PlayerStateManager playerStateManager;
    [SerializeField]
    GameTimeUI gameTimeUI;
    [SerializeField]
    PlayerInputManager playerInputManager;
    [SerializeField]
    PlayerSpriteManager playerSpriteManager;
    [SerializeField]
    PlayerMove playerMove;
    [SerializeField]
    PlayerPush playerPush;
    [SerializeField]
    PlayerStrike playerStrike;
    [SerializeField]
    PlayerData playerData;
    [SerializeField]
    PlayerDeath playerDeath;

    private void Awake()
    {
#if UNITY_EDITOR 
        if (isDebug == true)
        {
            GameSetting.BlockDataSettings = debugDataSettings;
            GameSetting.ProgressSettings = debugProgressSettings;
        }
#endif
        //マネージャーの初期化
        blockGenerator.InitializeSettings(spriteSettings, GameSetting.BlockDataSettings, blockManager);
        gameProgress.Initialize(GameSetting.ProgressSettings);
        blockManager.Initialize();
        highScoreManager.SetRankingKeysData(GameSetting.ProgressSettings.keysData);
        gameScore.Initialize(GameSetting.ProgressSettings.keysData.scoreType, blockManager, gameProgress);
        blockManager.AddUnbreakBlockAllErase(() => gameScore.AllErase());
        playerData.Initialize();
        playerDeath.AddDeathAnimationEndAction(gameProgress.GameEnd);
        blockGenerator.GameOver = gameProgress.GameEnd;
        gameProgress.AddGameEndAction(() => blockGenerator.IsGenerate = false);
        gameProgress.ProgressState.Subscribe(progressState => playerStateManager.IsMove = progressState == GameProgress.Game_Progress_State.In_Game);
        gameTimeUI.SetUp(gameProgress);
        //壊せないブロック生成
        blockGenerator.UnbreakableBlockGenerate();

        //プレイヤーの初期化
        playerMove.Initialize(playerData);
        playerStateManager.Initialize(blockManager);
        playerSpriteManager.Initialize();
        playerInputManager.Initialize();
        playerPush.Initialize();
        playerStrike.Initialize(gameScore);

        playerData.transform.position = playerData.GetPlayerPositionInWorld();
    }

    private void Start()
    {
        blockGenerator.SetUp(BlockManager.widthRange, BlockManager.heightRange);
        playerDeath.Initialize();
        gameProgress.GameStart();
    }
}
