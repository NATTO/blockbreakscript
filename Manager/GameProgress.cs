using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// ゲームの進行管理
/// </summary>
public class GameProgress : MonoBehaviour
{
    [SerializeField]
    ProgressSettings progressSettings;
    [SerializeField]
    Canvas gameStartCanvas;
    [SerializeField]
    Canvas gameEndCanvas;
    [SerializeField]
    StartTextAnimation startAnimation;
    List<Action> gameEndActions = new List<Action>();
    Coroutine gameProgressCoroutine;
    [SerializeField]
    float inGameTime = 0;
    public float InGameTime => inGameTime;
    static float inGameTimeScale = 1;
    public static float InGameTimeScale { set { inGameTimeScale = value; } get => inGameTimeScale; }
    ReactiveProperty<Game_Progress_State> progressState = new ReactiveProperty<Game_Progress_State>(Game_Progress_State.Start);
    public ReactiveProperty<Game_Progress_State> ProgressState => progressState;

    public void Initialize(ProgressSettings settings)
    {
        progressSettings = settings;
        inGameTime = progressSettings.inGameTime;
    }

    public void AddGameEndAction(Action value) => gameEndActions.Add(value);

    public void GameStart()
    {
        progressState.Value = Game_Progress_State.In_Game;
        gameProgressCoroutine = StartCoroutine(Progress());
        startAnimation.AnimationStart();
        inGameTimeScale = 1;
    }

    public void GameEnd()
    {
        progressState.Value = Game_Progress_State.End;
        gameEndCanvas.gameObject.SetActive(true);
        gameEndActions.ForEach(action => action.Invoke());
        StopCoroutine(gameProgressCoroutine);
    }

    IEnumerator Progress()
    {
        yield return new WaitForSeconds(progressSettings.gameStartTime);
        gameStartCanvas.gameObject.SetActive(false);

        while (inGameTime > 0)
        {
            yield return null;
            inGameTime -= Time.deltaTime * GameProgress.InGameTimeScale;
            if (inGameTime <= 0)
                inGameTime = 0;
        }
        GameEnd();
    }

    public enum Game_Progress_State
    {
        Start,
        In_Game,
        End
    }
}
