using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
/// <summary>
/// シーン読み込みクラス
/// </summary>
public class SceneLoader : SingletonMonoBehaviour<SceneLoader>
{
    const float loadSceneAnimationTime = 1f;
    [SerializeField]
    Image loadSceneImage;
    [SerializeField]
    Slider progressSlider;
    [SerializeField]
    float minLoadWaitTime;

    protected override void Awake()
    {
        base.Awake();
        Final();
    }


    void SetUp()
    {
        loadSceneImage.gameObject.SetActive(true);
        progressSlider.value = 0;
    }

    private void Final()
    {
        progressSlider.gameObject.SetActive(false);
        loadSceneImage.gameObject.SetActive(false);
    }

    public void LoadSceneAsync(string sceneName, Action sceneLoadCompleted = null)
        => StartCoroutine(LoadSceneAsyncCoroutine(sceneName, sceneLoadCompleted));

    Scene GetActiveSceneSelect()
    {
        Scene resultScene = default;
        for (int i = 0; i < SceneManager.sceneCount; i++)
        {
            var scene = SceneManager.GetSceneAt(i);
            if (scene.name == SceneNameManager.AwakeScene)
                continue;
            if (scene.name == SceneNameManager.DontDestroyOnLoad)
                continue;
            resultScene = scene;
        }
        return resultScene;
    }

    IEnumerator LoadSceneAsyncCoroutine(string sceneName, Action sceneLoadCompleted = null)
    {
        SetUp();
        var nowScene = SceneManager.GetActiveScene();

        if (nowScene.name == SceneNameManager.AwakeScene)
        {
            foreach (var i in nowScene.GetRootGameObjects())
            {
                Destroy(i);
            }
            nowScene = GetActiveSceneSelect();
        }
        //画面暗転
        var animation = LoadSceneAnimation(Load_Scene_Animation_Mode.Addtive);
        yield return animation;

        var unloadScene = SceneManager.UnloadSceneAsync(nowScene);
        bool isUnloadEnd = false;
        unloadScene.completed += (x) =>
        {
            isUnloadEnd = true;
        };
        while (isUnloadEnd == false)
        {
            yield return null;
        }

        //次のシーンをロード
        var asyncLoadOperation = SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive);
        var loadCoroutine = LoadSceneCoroutine(asyncLoadOperation);
        yield return loadCoroutine;

        while (animation.MoveNext()) yield return null;
        asyncLoadOperation.allowSceneActivation = true;

        while (true)
        {
            if (asyncLoadOperation.isDone)
            {
                sceneLoadCompleted?.Invoke();
                break;
            }
            yield return null;
        }

        Final();
    }

    IEnumerator LoadSceneCoroutine(AsyncOperation targetSceneOperation)
    {
        var loadTargetScene = targetSceneOperation;
        loadTargetScene.allowSceneActivation = false;
        float progressTime = 0;
        while (minLoadWaitTime > progressTime || loadTargetScene.progress != 0.9f)
        {
            progressTime += Time.deltaTime;
            progressSlider.value = loadTargetScene.progress;
            yield return null;
        }
        progressSlider.value = loadTargetScene.progress;
    }

    enum Load_Scene_Animation_Mode
    {
        Addtive,
        Subtractive
    }

    IEnumerator LoadSceneAnimation(Load_Scene_Animation_Mode mode)
    {
        float progressTime = 0;
        while (progressTime <= loadSceneAnimationTime)
        {
            switch (mode)
            {
                case Load_Scene_Animation_Mode.Addtive:
                    loadSceneImage.color = new Color(loadSceneImage.color.r, loadSceneImage.color.g, loadSceneImage.color.b, Mathf.Lerp(0, 1, progressTime));
                    break;
                case Load_Scene_Animation_Mode.Subtractive:
                    loadSceneImage.color = new Color(loadSceneImage.color.r, loadSceneImage.color.g, loadSceneImage.color.b, Mathf.Lerp(1, 0, progressTime));
                    break;
            }
            progressTime += Time.deltaTime * 2;
            yield return null;
        }
        progressSlider.gameObject.SetActive(true);
    }
}
