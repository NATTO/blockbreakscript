using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// ゲーム内の設定クラス
/// </summary>
public static class GameSetting
{
    public static BlockDataSettings BlockDataSettings;
    public static ProgressSettings ProgressSettings;
}
