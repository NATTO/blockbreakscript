using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// オブジェクトプールクラス
/// </summary>
/// <typeparam name="T"></typeparam>
public class MonobehaviourObjectPool<T> where T : MonoBehaviour
{
    List<T> objectList;
    T sampleObject;

    public MonobehaviourObjectPool(T sample, int objectCapacity)
    {
        sampleObject = sample;
        objectList = new List<T>(objectCapacity);
    }

    public T GetPoolObject()
    {
        var canUseObject = objectList.Find(x => x.gameObject.activeSelf == false);
        if(canUseObject == null)
        {
            canUseObject = CreateObject();
        }
        canUseObject.gameObject.SetActive(true);

        return canUseObject;
    }

    T CreateObject()
    {
        var resultObject = MonoBehaviour.Instantiate(sampleObject);
        objectList.Add(resultObject);
        return resultObject;
    }
}
