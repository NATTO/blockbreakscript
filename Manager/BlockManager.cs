using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
/// <summary>
/// ブロック管理クラス
/// </summary>
public class BlockManager : MonoBehaviour
{
    public const int widthRange = 10;
    public const int heightRange = 7;
    const float blockBreakAnimationTime = 1.5f;
    const int breakBlockCount = 2;

    [SerializeField]
    float blockBreakFlashingTime;

    /// <summary>
    /// 今あるブロック、落下中はここに含まない
    /// </summary>
    BlockBase[][] blockList = new BlockBase[widthRange][];
    public BlockBase[][] BlockList => blockList;
    List<Action<bool>> falledBlockAction = new List<Action<bool>>();
    List<Action> unbreakBlockAllEraseAction = new List<Action>();

    public void Initialize()
    {
        //横幅分初期化する
        for (var i = 0; i < widthRange; i++)
        {
            blockList[i] = new BlockBase[heightRange];
        }
        StartCoroutine(FalledBlockCheck());
    }

    /// <summary>
    /// ブロックが落ちた時のアクションを追加する
    /// 引数のboolは落ちたブロックが消えたらtrue、消えなかったらfalse
    /// </summary>
    /// <param name="action"></param>
    public void AddFalledBlockAction(Action<bool> action)
    {
        falledBlockAction.Add(action);
    }

    /// <summary>
    /// 壊せないブロックを全部消した時のアクションを追加する
    /// </summary>
    /// <param name="action"></param>
    public void AddUnbreakBlockAllErase(Action action)
    {
        unbreakBlockAllEraseAction.Add(action);
    }

    bool isFalledBlock = false;

    public void FalledBlock(BlockBase fallBlock)
    {
        isFalledBlock = true;
        fallBlockList.RemoveValue(fallBlock);
    }

    IEnumerator FalledBlockCheck()
    {
        while (true)
        {
            yield return null;
            if (!isFalledBlock) continue;

            bool isBlockBreak = BlockBreak();
            falledBlockAction.ForEach(x => x?.Invoke(isBlockBreak));
            isFalledBlock = false;
        }
    }

    public void SetBlock(int width, int height, BlockBase value)
    {
        if (blockList[width][height] != null && value != null)
        {
            Debug.Log("既にブロックがある");
            SetBlock(width, height + 1, blockList[width][height]);
        }
        blockList[width][height] = value;
        if (value != null)
            value.transform.position = new Vector2(BlockGenerator.zeroPoint.x + BlockGenerator.blockSpan * width, BlockGenerator.zeroPoint.y + (BlockGenerator.blockSpan * height));
    }

    public EventHandleHashSet<BlockBase> fallBlockList = new EventHandleHashSet<BlockBase>();

    List<Action<int>> blockBreakActions = new List<Action<int>>();
    public void AddBlockBreakAction(Action<int> value) => blockBreakActions.Add(value);


    /// <summary>
    /// 指定列の空いてる場所を取得
    /// </summary>
    /// <param name="width"></param>
    /// <returns></returns>
    public int GetMinBlankBlockHeight(int width)
    {
        var blankBlocks = blockList[width].Select((block, index) => (block, index)).Where(value => value.block == null);
        var fallenBlocks = GetFallenBlock(width);
        if (!blankBlocks.Any())
        {
            //空いてるブロック無かったらRange渡す
            return heightRange;
        }
        var fallenBlock = GetMinFallenBlock(width);
        if (fallenBlock != null)
        {
            //落ちてるブロックがあったらそれの高さを渡す
            int minBlankBlockHeight = blankBlocks.Min(blankBlocks => blankBlocks.index);
            if (minBlankBlockHeight == fallenBlock.Height)
            {
                return minBlankBlockHeight + 1;
            }
            return fallenBlock.Height > minBlankBlockHeight ? minBlankBlockHeight : fallenBlock.Height;
        }
        else
            return blankBlocks.Min(blankBlocks => blankBlocks.index);
    }

    public BlockBase GetMinFallenBlock(int width)
    {
        var blocks = GetFallenBlock(width);
        if (!blocks.Any()) return null;
        int minHeightValue = blocks.Min(minBlock => minBlock.Height);

        return blocks.Single(block => block.Height == minHeightValue);
    }

    /// <summary>
    /// 落ちるブロックを列指定で取る。
    /// </summary>
    /// <param name="width"></param>
    /// <returns></returns>
    public IEnumerable<BlockBase> GetFallenBlock(int width)
    {
        return fallBlockList.Value.Where(block => block.Width == width);
    }

    public BlockBase GetBlock(int width, int height)
    {
        if (width < 0 || height < 0 || heightRange <= height || widthRange <= width)
        {
            return null;
        }

        return blockList[width][height];
    }

    /// <summary>
    /// 全ブロック見てブロックがそろってたら消す
    /// ブロックの状況が変わったら呼び出す
    /// </summary>
    public bool BlockBreak()
    {
        List<BlockBase> breakList = new List<BlockBase>();
        for (int i = 0; i < widthRange; i++)
        {
            foreach (var block in blockList[i])
            {
                if (block == null) continue;
                if (breakList.Contains(block)) { continue; }
                List<BlockBase> searchBlock = GetSameBlocks(i, block.Height, block.BlockType);
                if (searchBlock.Count == 0) continue;

                List<BlockBase> hitBlocks = new List<BlockBase>();
                hitBlocks.AddRange(searchBlock);
                foreach (var nextBlock in searchBlock)
                {
                    foreach (var addBlock in GetSameBlocks(nextBlock.Width, nextBlock.Height, nextBlock.BlockType))
                    {
                        if (addBlock != block)
                        {
                            hitBlocks.Add(addBlock);
                        }
                    }
                }

                if (hitBlocks.Count >= breakBlockCount)
                {
                    hitBlocks.ForEach(block => { if (!breakList.Contains(block)) { breakList.Add(block); } });
                    if (!breakList.Contains(block))
                        breakList.Add(block);
                }
            }
        }

        StartCoroutine(BlockBreakCoroutine(breakList));

        return breakList.Any();
    }

    /// <summary>
    /// ブロックが壊れる行程
    /// </summary>
    /// <param name="breakBlocks"></param>
    /// <returns></returns>
    IEnumerator BlockBreakCoroutine(List<BlockBase> breakBlocks)
    {
        if (breakBlocks.Count == 0) yield break;
        float animProgressTime = 0;
        float flashTime = 0;
        List<SpriteRenderer> breakBlocksList = breakBlocks.ConvertAll(block => block.GetComponent<SpriteRenderer>()).ToList();
        GameProgress.InGameTimeScale = 0;
        //アニメーション
        while (animProgressTime < blockBreakAnimationTime)
        {
            if (breakBlocksList.Any(block => block == null)) break;
            if (flashTime > blockBreakFlashingTime)
            {
                breakBlocksList.ForEach(block => block.enabled = !block.enabled);
                flashTime = 0;
            }
            yield return null;
            animProgressTime += Time.deltaTime;
            flashTime += Time.deltaTime;
        }
        //破壊出来ないブロックが無い時の処理
        if (!blockList.Any(colmnBlockList => colmnBlockList.Any(block => { if (block == null) return false; return block.IsUnBreak; })))
            unbreakBlockAllEraseAction.ForEach(action => action?.Invoke());

        blockBreakActions.ForEach(action => action?.Invoke(breakBlocks.Count));
        //消す処理
        foreach (var breakBlock in breakBlocks)
        {
            blockList[breakBlock.Width][breakBlock.Height] = null;
            if (breakBlock != null)
                breakBlock.gameObject.SetActive(false);
        }
        AllFall();
        GameProgress.InGameTimeScale = 1;

    }

    /// <summary>
    /// 指定した同じブロックと同じタイプのものを返す
    /// </summary>
    /// <param name="width"></param>
    /// <param name="height"></param>
    /// <param name="searchType"></param>
    /// <returns></returns>
    /// <exception cref="Exception"></exception>
    List<BlockBase> GetSameBlocks(int width, int height, Block_Type searchType)
    {
        List<BlockBase> hitBlocks = new List<BlockBase>();
        if (width < 0 || width > widthRange || height < 0 || height > heightRange)
            throw new Exception($"[GetSameBlocks]横か縦が範囲外{width} / {height}");

        HitBlockAddHitBlocks(GetBlock(width - 1, height));
        HitBlockAddHitBlocks(GetBlock(width + 1, height));
        HitBlockAddHitBlocks(GetBlock(width, height - 1));
        HitBlockAddHitBlocks(GetBlock(width, height + 1));
        void HitBlockAddHitBlocks(BlockBase hitBlock)
        {
            if (hitBlock != null && hitBlock.BlockType == searchType)
            {
                hitBlocks.Add(hitBlock);
            }
        }
        return hitBlocks;
    }

    /// <summary>
    /// 下にブロックが無いやつを落とす
    /// </summary>
    public void AllFall()
    {
        foreach (var rowBlocks in blockList.Select((value, length) => new { value, length }))
        {
            IEnumerable<BlockBase> blocks = rowBlocks.value.ToList().Where(block => block != null).OrderBy(block => block.Height);
            IEnumerable<BlockBase> fallBlocks = GetFallenBlock(rowBlocks.length).OrderBy(block => block.Height);
            blocks = blocks.Concat(fallBlocks);
            List<int> heights = new List<int>();
            int minBlankBlockHeight = GetMinBlankBlockHeight(rowBlocks.length);

            foreach (var block in blocks)
            {
                if (block.Height < minBlankBlockHeight)
                    continue;
                block.UpdateFallEndHeight(minBlankBlockHeight);
                heights.Add(block.Height);
                minBlankBlockHeight += 1;
            }
        }
    }

    public int GetHeightCount(int width)
    {
        var fallBlocks = fallBlockList.Value.Where(block => block.Width == width);
        if (fallBlocks.Any())
        {
            return fallBlocks.Max(block => block.Height) + 1;
        }
        return GetMinBlankBlockHeight(width);
    }
#if UNITY_EDITOR
    public void CreateBlock(int width, int height, BlockBase block)
    {
        if (block == null)
        {
            return;
        }
        var nowBlock = GetBlock(width, height);
        if (nowBlock != null)
        {
            Destroy(nowBlock);
            blockList[width][height] = null;
        }
        var instance = Instantiate(block, new Vector3(BlockGenerator.zeroPoint.x + (width * BlockGenerator.blockSpan),
            BlockGenerator.zeroPoint.y + height, 0), Quaternion.identity, GameObject.Find("BlockParentObject").transform);
        blockList[width][height] = instance;
        AllFall();
    }
#endif
}
