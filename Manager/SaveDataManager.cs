using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using UnityEngine;
using NCMB;
using System;

/// <summary>
/// セーブデータ管理クラス
/// </summary>
public static class SaveDataManager
{
#if UNITY_EDITOR || PLATFORM_STANDALONE_WIN 
    static readonly string SaveFilePath = $"C:/Users/{Environment.UserName}/AppData/LocalLow/{Application.companyName}/{Application.productName}/savedata{{0}}.txt";
#endif
#if !UNITY_EDITOR && UNITY_ANDROID
    static readonly string SaveFilePath = $"{Application.persistentDataPath}/{Application.companyName}/{Application.productName}/savedata{{0}}.txt";
#endif
    const string aesPass = "Sjt93KjnwpGykgnF";
    const string aesSalt = "SKTngf94OTU5026u";
    static ScoreData loadData = null;
    public static ScoreData LoadData => loadData;


    public static ScoreData LoadScoreData(ScoreType scoreType)
    {
        if (loadData != null)  return loadData; 
        byte[] saveData = null;
        string filePath = string.Format(SaveFilePath,scoreType.ToString());

        if (File.Exists(filePath))
        {
            using (StreamReader saveDataFile = new StreamReader(filePath))
            {
                saveData = Convert.FromBase64String(saveDataFile.ReadToEnd());
                saveDataFile.Close();
            }
        }
        else return null;

        #region 複合化処理
        RijndaelManaged rijndael = new RijndaelManaged();
        rijndael.KeySize = 128;
        rijndael.BlockSize = 128;

        byte[] saltBytes = Encoding.UTF8.GetBytes(aesSalt);
        Rfc2898DeriveBytes deriveBytes = new Rfc2898DeriveBytes(aesPass, saltBytes);
        deriveBytes.IterationCount = 1000;
        rijndael.Key = deriveBytes.GetBytes(rijndael.KeySize / 8);
        rijndael.IV = deriveBytes.GetBytes(rijndael.BlockSize / 8);

        ICryptoTransform decryptor = rijndael.CreateDecryptor();
        byte[] plain = decryptor.TransformFinalBlock(saveData, 0, saveData.Length);
        ScoreData saveDataScore = JsonUtility.FromJson<ScoreData>(Encoding.UTF8.GetString(plain));
        loadData = saveDataScore;
        #endregion

        return saveDataScore;
    }

    public static void SaveScoreData(ScoreData data)
    {
        loadData = data;
        string scoreJsonData = data.GetJsonData();
        RijndaelManaged rijndael = new RijndaelManaged();
        rijndael.KeySize = 128;
        rijndael.BlockSize = 128;
        byte[] scoreBytes = Encoding.UTF8.GetBytes(scoreJsonData);

        #region 暗号化処理
        byte[] saltBytes = Encoding.UTF8.GetBytes(aesSalt);
        Rfc2898DeriveBytes deriveBytes = new Rfc2898DeriveBytes(aesPass, saltBytes);
        deriveBytes.IterationCount = 1000;

        rijndael.Key = deriveBytes.GetBytes(rijndael.KeySize / 8);
        rijndael.IV = deriveBytes.GetBytes(rijndael.BlockSize / 8);

        ICryptoTransform encryptor = rijndael.CreateEncryptor();
        byte[] encrypted = encryptor.TransformFinalBlock(scoreBytes, 0, scoreBytes.Length);

        var base64EncryptedValue = Convert.ToBase64String(encrypted);

        if(!Directory.Exists(SaveFilePath))
        {
            Directory.CreateDirectory(SaveFilePath);
        }

        File.WriteAllText(string.Format(SaveFilePath,data.scoreType.ToString()), base64EncryptedValue);

        encryptor.Dispose();
        #endregion
    }
}
