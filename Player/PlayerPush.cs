using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

/// <summary>
/// ブロックを動かすクラス
/// </summary>
public class PlayerPush : MonoBehaviour
{
    [SerializeField]
    private float timeToPush = 0.3f;
    
    [SerializeField]
    private float timeToIdle = 0.3f;
    
    public void Initialize()
    {
    }

    public IEnumerator PushCoroutine(BlockBase fowerdBlock, BlockBase destinationBlock,int width,int direction)
    {
        //前のブロックが空いてたり動かす先が範囲外だったら抜ける
        if (fowerdBlock == null || width + direction * 2 < 0 || width + direction * 2 >= PlayerData.xMoveLimit)
            yield break;
        //動かす先にブロックがあったら抜ける
        if (destinationBlock != null)
            yield break;

        float time = 0;
        while (time < timeToPush)
        {
            yield return null;
            time += Time.deltaTime * GameProgress.InGameTimeScale;
        }
        yield return fowerdBlock.SideMove(direction);
        time = 0;
        while (time < timeToIdle)
        {
            yield return null;
            time += Time.deltaTime * GameProgress.InGameTimeScale;
        }
    }
}
