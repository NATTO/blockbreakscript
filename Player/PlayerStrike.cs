using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// ブロックを壊すクラス
/// </summary>
public class PlayerStrike : MonoBehaviour
{
    [SerializeField]
    private float timeToStrike = 0.5f;
    [SerializeField]
    private float timeToIdle = 0.3f;

    GameScore gameScore;

    public void Initialize(GameScore score)
    {
        gameScore = score;
    }

    public IEnumerator StrikePreparationCoroutine()
    {
        float time = 0;

        while (time < timeToStrike)
        {
            yield return null;
            time += Time.deltaTime * GameProgress.InGameTimeScale;
        }
    }

    public IEnumerator StrikeCoroutine(BlockBase fowerdBlock)
    {
        if (fowerdBlock == null)
            yield break;
        if (fowerdBlock.IsUnBreak == true) yield break;
        float time = 0;

        while (time < timeToStrike)
        {
            yield return null;
            time += Time.deltaTime * GameProgress.InGameTimeScale;
        }
        bool isBreak = fowerdBlock.Strike();
        if(isBreak == true)
        {
            switch (fowerdBlock.BlockType)
            {
                case Block_Type.Gray:
                    gameScore.IncreaseScore(GameScore.grayBlockBreakScore);
                    break;
                default:
                    gameScore.IncreaseScore(GameScore.blockBreakScore);
                    break;
            }
        }
        time = 0;
        while (time < timeToIdle)
        {
            yield return null;
            time += Time.deltaTime * GameProgress.InGameTimeScale;
        }

    }
}
