using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(PlayerDeath))]
[RequireComponent(typeof(PlayerMove))]
[RequireComponent(typeof(PlayerPush))]
[RequireComponent(typeof(PlayerStrike))]
/// <summary>
/// プレイヤーの状態管理
/// </summary>
public class PlayerStateManager : MonoBehaviour
{
    public enum State
    {
        Idle,
        Move_X,
        Move_Y,
        Move_Y_UP,
        Move_Y_Down,
        Strike_X_Pre,
        Strike_X,
        Strike_Y_Pre,
        Strike_Y,
        Push,
        Death,
        Follow_Through
    }

    ReactiveProperty<State> nowState;
    public ReactiveProperty<State> NowState => nowState;

    [SerializeField]
    PlayerMove playerMove;
    [SerializeField]
    PlayerPush playerPush;
    [SerializeField]
    PlayerStrike playerStrike;
    [SerializeField]
    PlayerDeath playerDeath;
    [SerializeField]
    PlayerData playerData;

    BlockManager blockManager;

    public bool IsMove { get; set; } = false;

    public void Initialize(BlockManager blockManager)
    {
        nowState = new ReactiveProperty<State>(State.Idle);
        playerDeath.AddDeathAction(() => nowState.Value = State.Death);
        this.blockManager = blockManager;
        StartCoroutine(PlayerDeathCoroutine());
    }

    public void SetState(State state, int moveX = 0, int moveY = 0)
    {
        if ((nowState.Value == State.Idle || nowState.Value == State.Follow_Through) && IsMove == true)
            StartCoroutine(PlayerActionCoroutine(state, moveX, moveY));
    }

    IEnumerator PlayerDeathCoroutine()
    {
        while(playerDeath.IsDeath == false)
        {
            if (playerDeath.IsDeathCollision(blockManager?.GetFallenBlock(playerData.PlayerPosition.x))) playerDeath.Death();
            yield return null;
        }
    }

    IEnumerator PlayerActionCoroutine(State state, int moveX = 0, int moveY = 0)
    {
        ///<summary>プレイヤーの向き。更新が移動時なのでこの形に</summary>
        int PlayerDirection() => moveX != 0 ? moveX : playerData.playerDirection;
        bool IsCollisionBlock(Vector2 nextPosition) => blockManager.GetBlock((int)nextPosition.x, (int)nextPosition.y) == null;
        switch (state)
        {
            //待機状態
            case State.Idle:
                break;
            //下方向（一応上もできるけど上にブロックあったら死ぬ）殴り
            case State.Strike_Y:
                nowState.Value = State.Strike_Y_Pre;
                yield return playerStrike.StrikePreparationCoroutine();
                nowState.Value = State.Strike_Y;
                yield return playerStrike.StrikeCoroutine(blockManager.GetBlock(playerData.PlayerPosition.x, playerData.PlayerPosition.y + moveY));
                nowState.Value = State.Idle;
                break;
            //横方向殴り
            case State.Strike_X:
                nowState.Value = State.Strike_X_Pre;
                yield return playerStrike.StrikePreparationCoroutine();
                nowState.Value = State.Strike_X;
                yield return playerStrike.StrikeCoroutine(blockManager.GetBlock(playerData.PlayerPosition.x + PlayerDirection(), playerData.PlayerPosition.y));
                nowState.Value = State.Idle;
                break;
            //ブロック押し
            case State.Push:
                nowState.Value = State.Push;
                yield return playerPush.PushCoroutine(blockManager.GetBlock(playerData.PlayerPosition.x + PlayerDirection(), playerData.PlayerPosition.y),
                    blockManager.GetBlock(playerData.PlayerPosition.x + PlayerDirection() * 2, playerData.PlayerPosition.y),
                    playerData.PlayerPosition.x, PlayerDirection());
                nowState.Value = State.Idle;
                break;
            //横方向移動
            case State.Move_X:
                playerData.playerDirection = moveX;
                nowState.Value = State.Move_X;
                Vector2Int moveXValue = playerData.PlayerPosition + Vector2Int.right * moveX;
                if (moveXValue.x < PlayerData.xMoveLimit && moveXValue.x > -1 
                    && IsCollisionBlock(moveXValue) && playerMove.IsMoveX(moveX, blockManager.GetFallenBlock(moveXValue.x)))
                {
                    yield return playerMove.MoveXCoroutine(moveX);
                }
                nowState.Value = State.Follow_Through;
                //移動終わった時すぐ待機状態の絵に戻らないよう1フレーム待つ
                yield return null;
                if (nowState.Value == State.Follow_Through)
                    nowState.Value = State.Idle;
                break;
            //縦方向移動
            case State.Move_Y:
                if (moveY > 0)
                    nowState.Value = State.Move_Y_UP;
                else if (moveY < 0)
                    nowState.Value = State.Move_Y_Down;
                Vector2 moveYValue = playerData.PlayerPosition + Vector2.up * moveY;
                if (moveYValue.y < PlayerData.yMoveLimit && moveYValue.y > -1 && IsCollisionBlock(moveYValue) && playerMove.IsMoveY(moveY))
                    yield return playerMove.MoveYCoroutine(moveY);
                nowState.Value = State.Follow_Through;
                yield return null;
                //移動終わった時すぐ待機状態の絵に戻らないよう1フレーム待つ
                if (nowState.Value == State.Follow_Through)
                    nowState.Value = State.Idle;
                break;
        }
    }
}