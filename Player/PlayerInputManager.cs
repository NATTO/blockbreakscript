using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// プレイヤーのインプットを状態管理クラスに投げるクラス
/// </summary>
public class PlayerInputManager : MonoBehaviour
{
    IPlayerInput input;
    [SerializeField]
    PlayerStateManager playerStateManager;

    public void Initialize()
    {
        input = GetComponent<IPlayerInput>();
    }


    void Update()
    {
        input.InputUpdate();
        if (input.GetStrike())
        {
            if (input.GetVertical() != 0)
            {
                playerStateManager.SetState(PlayerStateManager.State.Strike_Y, 0, input.GetVertical());
                return;
            }

            playerStateManager.SetState(PlayerStateManager.State.Strike_X, input.GetHorizontal(), 0);            
            return;
        }
        if (input.GetPush())
        {
            playerStateManager.SetState(PlayerStateManager.State.Push, input.GetHorizontal());
            return;
        }
        if (input.GetVertical() != 0)
        {
            playerStateManager.SetState(PlayerStateManager.State.Move_Y, 0, input.GetVertical());
        }
        if (input.GetHorizontal() != 0)
        {
            playerStateManager.SetState(PlayerStateManager.State.Move_X, input.GetHorizontal());
        }
    }
}
