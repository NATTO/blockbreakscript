using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

/// <summary>
/// スマホ用の入力クラス
/// </summary>
public class PlayerTouchInput : MonoBehaviour, ITouchInput, IPlayerInput
{
    Touch[] touches = new Touch[2] { default, default };
    public Touch[] Touches => touches;
    [SerializeField]
    Image upImage;
    [SerializeField]
    Image downImage;
    [SerializeField]
    Image rightImage;
    [SerializeField]
    Image leftImage;
    [SerializeField]
    Image pushImage;
    [SerializeField]
    Image strikeImage;
    PointerEventData firstPointData;
    PointerEventData secondPointData;
    List<RaycastResult> firstResults = new List<RaycastResult>();
    List<RaycastResult> secondResults = new List<RaycastResult>();
    EventSystem currentEventSystem = null;
    [SerializeField]
    int horizontal = 0;
    [SerializeField]
    int vertical = 0;
    [SerializeField]
    bool push = false;
    [SerializeField]
    bool strike = false;

    void Start()
    {
        currentEventSystem = EventSystem.current;
        firstPointData = new PointerEventData(currentEventSystem);
        secondPointData = new PointerEventData(currentEventSystem);
    }

    void IPlayerInput.InputUpdate()
    {

#if UNITY_ANDROID && !UNITY_EDITOR
        horizontal = 0;
        vertical = 0;
        push = false;
        strike = false;
        if (Input.touchCount == 0) return;

        firstResults.Clear();
        secondResults.Clear();
        touches[0] = Input.GetTouch(0);
        firstPointData.position = touches[0].position;
        currentEventSystem.RaycastAll(firstPointData, firstResults);

        if (Input.touchCount > 1)
        {
            touches[1] = Input.GetTouch(1);
            secondPointData.position = touches[1].position;
            currentEventSystem.RaycastAll(secondPointData, secondResults);
        }
        else touches[1] = default;

        foreach (var firstResult in firstResults)
        {
            Touch(touches[0], firstResult.gameObject.name);
        }
        foreach (var secondResult in secondResults)
        {
            Touch(touches[1], secondResult.gameObject.name);
        }

        void Touch(Touch value, string objectName)
        {
            if (value.phase == TouchPhase.Began)
            {
                TouchAction(objectName);
            }
            if (value.phase == TouchPhase.Began || value.phase == TouchPhase.Moved || value.phase == TouchPhase.Stationary)
            {
                TouchMove(objectName);
            }
        }

        void TouchMove(string objectName)
        {
            if (objectName == upImage.name)
                vertical = 1;
            if (objectName == downImage.name)
                vertical = -1;
            if (objectName == rightImage.name)
                horizontal = 1;
            if (objectName == leftImage.name)
                horizontal = -1;
        }

        void TouchAction(string objectName)
        {
            if (objectName == pushImage.name)
                push = true;
            if (objectName == strikeImage.name)
                strike = true;
        }
#endif
#if UNITY_EDITOR

        
        bool mouseButtonDown = Input.GetMouseButtonDown(0);
        bool mouseButton = Input.GetMouseButton(0);

        firstPointData.position = Input.mousePosition;
        currentEventSystem.RaycastAll(firstPointData, firstResults);
        horizontal = 0;
        vertical = 0;
        push = false;
        strike = false;
        foreach (var result in firstResults)
        {
            string resultName = result.gameObject.name;

            if (mouseButton)
            {
                if (resultName == upImage.name)
                    vertical = 1;
                if (resultName == downImage.name)
                    vertical = -1;
                if (resultName == rightImage.name)
                    horizontal = 1;
                if (resultName == leftImage.name)
                    horizontal = -1;
            }
            if (mouseButtonDown)
            {
                if (resultName == pushImage.name)
                    push = true;
                if (resultName == strikeImage.name)
                    strike = true;

            }
        }
#endif
    }

    int IPlayerInput.GetHorizontal() => horizontal;

    bool IPlayerInput.GetPush() => push;

    bool IPlayerInput.GetStrike() => strike;

    int IPlayerInput.GetVertical() => vertical;
}

internal interface ITouchInput
{
    Touch[] Touches { get; }
}