using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
/// <summary>
/// プレイヤーの移動クラス
/// </summary>
public class PlayerMove : MonoBehaviour
{
    [SerializeField]
    float moveTime = 0.2f;
    [SerializeField]
    Vector2Int initializePlayerPosition;
    PlayerData playerData;

    public void Initialize(PlayerData data)
    {
        playerData = data;
        playerData.PlayerPosition = initializePlayerPosition;
    }

    /// <summary>
    /// X方向へ動けるか
    /// </summary>
    /// <param name="moveValue">-1か1を指定</param>
    public bool IsMoveX(int moveValue,IEnumerable<BlockBase> fallBlocks)
    {
        //ブロックにぶつかってたり範囲外だったりすると移動できない
        if (playerData.PlayerPosition.x + moveValue > PlayerData.xMoveLimit || playerData.PlayerPosition.x + moveValue < 0)
            return false;
        if (fallBlocks.Any())
        {
            var nearPlayerFallBlock = fallBlocks.FirstOrDefault(block =>
            {
                if (block == null) return default;
                float blockHeight = block.transform.position.y;
                float playerHeight = playerData.GetPlayerPositionInWorld().y;
                return (blockHeight - playerHeight) <= 4f;
            });
            if (nearPlayerFallBlock != null) return false;
        }

        return true;
    }

    /// <summary>
    /// Y方向へ動けるか
    /// </summary>
    /// <param name="moveValue">-1か1を指定</param>
    public bool IsMoveY(int moveValue)
    {
        if (playerData.PlayerPosition.y + moveValue > PlayerData.yMoveLimit || playerData.PlayerPosition.y + moveValue < 0)
            return false;

        return true;
    }

    public IEnumerator MoveXCoroutine(int moveValue)
    {
        playerData.PlayerPosition += Vector2Int.right * moveValue;
        yield return MoveCoroutine(playerData.PlayerPosition);
    }

    public IEnumerator MoveYCoroutine(int moveValue)
    {
        playerData.PlayerPosition += Vector2Int.up * moveValue;
        yield return MoveCoroutine(playerData.PlayerPosition);
    }

    /// <summary>
    /// 動くアニメーション
    /// </summary>
    /// <param name="movePosition"></param>
    /// <returns></returns>
    IEnumerator MoveCoroutine(Vector2 movePosition)
    {
        Vector2 basePosition = BlockGenerator.zeroPoint;
        float ratio = BlockGenerator.blockSpan;
        float time = 0;
        Vector3 nowPosition = transform.position;
        Vector3 toPosition = new Vector2(basePosition.x + (ratio * movePosition.x), basePosition.y + (ratio * movePosition.y));

        while (time <= 1)
        {
            transform.position = Vector3.Lerp(nowPosition, toPosition, time);
            yield return null;
            time += Time.deltaTime * GameProgress.InGameTimeScale / moveTime;
        }

        transform.position = toPosition;
    }

}
