using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// プレイヤーの絵変更するクラス
/// </summary>
public class PlayerSpriteManager : MonoBehaviour
{
    [SerializeField]
    PlayerStateManager playerStateManager;
    [SerializeField]
    SpriteRenderer spriteRenderer;
    [SerializeField]
    Sprite moveHorizontalStartSprite;
    [SerializeField]
    Sprite moveHorizontalEndSprite;
    [SerializeField]
    Sprite moveVerticalUpSprite;
    [SerializeField]
    Sprite moveVerticalDownSprite;
    [SerializeField]
    Sprite preStrikeSprite;
    [SerializeField]
    Sprite strikeSprite;
    [SerializeField]
    Sprite pushSprite;
    [SerializeField]
    Sprite idleSprite;
    [SerializeField]
    PlayerData playerData;

    public void Initialize()
    {
        playerStateManager.NowState.Subscribe(StateChange);
    }

    void StateChange(PlayerStateManager.State state)
    {
        switch (state)
        {
            case PlayerStateManager.State.Move_X:
                if (playerData.playerDirection > 0)
                    spriteRenderer.flipX = false;
                else spriteRenderer.flipX = true;
                if(spriteRenderer.sprite == moveHorizontalStartSprite)
                    spriteRenderer.sprite = moveHorizontalEndSprite;
                else spriteRenderer.sprite = moveHorizontalStartSprite;
                break;

            case PlayerStateManager.State.Strike_X_Pre:
                spriteRenderer.sprite = preStrikeSprite;
                break;
            case PlayerStateManager.State.Move_Y_UP:
                spriteRenderer.sprite = moveVerticalUpSprite;
                break;
            case PlayerStateManager.State.Move_Y_Down:
                spriteRenderer.sprite = moveVerticalDownSprite;
                break;
            case PlayerStateManager.State.Push:
                spriteRenderer.sprite = pushSprite;
                break;
            case PlayerStateManager.State.Strike_X:
                spriteRenderer.sprite = strikeSprite;
                break;
            case PlayerStateManager.State.Strike_Y:

                break;
            case PlayerStateManager.State.Death:

                break;
            case PlayerStateManager.State.Idle:
                spriteRenderer.sprite = idleSprite;
                break;
        }
    }
}
