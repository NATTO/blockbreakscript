using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;
/// <summary>
/// 死を管理するクラス
/// </summary>
public class PlayerDeath : MonoBehaviour
{
    const float deathDistance = 1;
    [SerializeField]
    bool isDebugInvicible;
    bool isDeath;

    public bool IsDeath { get { return isDeath; } }

    List<Action> deathAction = new List<Action>();
    List<Action> deathAnimationEndAction = new List<Action>();

    public void AddDeathAction(Action value) => deathAction.Add(value);

    public void AddDeathAnimationEndAction(Action value) => deathAnimationEndAction.Add(value);

    public void Initialize()
    {
    }

    public bool IsDeathCollision(IEnumerable<BlockBase> fallBlocks)
    {
        if (fallBlocks == null) return false;
        if (fallBlocks.Count() == 0) return false;

        foreach (var fallBlock in fallBlocks)
        {
            bool deathCondition = Vector3.Distance(transform.position, fallBlock.transform.position) <= deathDistance
                && isDeath == false && isDebugInvicible == false;
            if (deathCondition) return true;
        }
        return false;
    }

    public void Death()
    {
        isDeath = true;
        deathAction?.ForEach(x => x?.Invoke());
        StartCoroutine(DeathAnimation());
    }

    IEnumerator DeathAnimation()
    {
        Transform playerTransform = GetComponent<Transform>();
        Vector3 localScale = playerTransform.localScale;
        Vector3 deathScale = new Vector3(1, 0.1f, 1);
        float progress = 0f;
        GameProgress.InGameTimeScale = 0;
        SoundManager.Instance.SEPlay(SE_ID.PLAYER_DEATH);

        while (progress <= 1f)
        {
            progress += Time.deltaTime;
        }

        progress = 0;

        while (progress <= 1)
        {
            Vector3 nowScale = Vector3.Lerp(localScale, deathScale, progress);
            playerTransform.localScale = nowScale;
            yield return null;
            progress += Time.deltaTime * 2.5f;
        }
        GameProgress.InGameTimeScale = 1;
        deathAnimationEndAction?.ForEach(action => action?.Invoke());
    }
}
