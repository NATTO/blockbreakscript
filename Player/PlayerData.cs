using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// プレイヤーのデータクラス
/// </summary>
public class PlayerData : MonoBehaviour
{
    public int playerDirection { get; set; } = 1;

    public void Initialize()
    {
    }

    public const int xMoveLimit = 10;
    public const int yMoveLimit = 7;

    public Vector2Int PlayerPosition { get; set; }

    public Vector2 GetPlayerPositionInWorld()
    {
        return new Vector2(BlockGenerator.zeroPoint.x + (BlockGenerator.blockSpan * PlayerPosition.x), BlockGenerator.zeroPoint.y + (BlockGenerator.blockSpan * PlayerPosition.y));
    }
}
