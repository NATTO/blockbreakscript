using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// PC用のインプットクラス
/// </summary>
public class PlayerPCInput : MonoBehaviour,IPlayerInput
{
    int horizontal;
    int vertical;
    bool push;
    bool strike;

    void IPlayerInput.InputUpdate()
    {
        push = Input.GetMouseButtonDown(0);
        strike = Input.GetMouseButtonDown(1);
        horizontal = (int)Input.GetAxisRaw("Horizontal");
        vertical = (int)Input.GetAxisRaw("Vertical");
    }

    int IPlayerInput.GetHorizontal() => horizontal;

    bool IPlayerInput.GetPush()=> push;

    bool IPlayerInput.GetStrike() => strike;

    int IPlayerInput.GetVertical() => vertical;
}
