using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// プレイヤーのインプット基底インターフェース
/// </summary>
public interface IPlayerInput
{
    /// <summary>
    /// 今フレームのインプットを取る
    /// </summary>
    void InputUpdate();
    int GetHorizontal();
    int GetVertical();
    bool GetStrike();
    bool GetPush();
}
