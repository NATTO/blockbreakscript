using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NCMB;
using NCMB.Internal;
using System;
/// <summary>
/// ランキング管理クラス
/// </summary>
public static class Ranking
{
    public const string playerNameKey = "name";
    public const string scoreKey = "score";

    public static IEnumerator<List<ScoreData>> GetRanking(string rankingClassName, int limit, int skipRank = 0)
    {
        NCMBQuery<NCMBObject> query = new NCMBQuery<NCMBObject>(rankingClassName);
        query.Skip = skipRank;
        query.Limit = limit;
        query.AddDescendingOrder(scoreKey);

        List<ScoreData> resultData = new List<ScoreData>();
        int getResult = 0;
        //ニフティクラウドのデータ取得
        query.FindAsync((List<NCMBObject> getData, NCMBException exception) =>
        {
            if (exception == null)
            {
                resultData = getData.ConvertAll(value =>
                {
                    string name = (string)value[playerNameKey];
                    uint score = Convert.ToUInt32(value[scoreKey]);
                    var result = new ScoreData(name,score);
                    return result;
                });
                getResult++;
            }
            else getResult--;
        });
        while (getResult == 0)
        {
            yield return null;
        }
        if (getResult == 1)
            yield return resultData;
    }

    public static IEnumerator<int> SetRanking(string rankingClassName, ScoreData data)
    {
        NCMBObject rankingObject = new NCMBObject(rankingClassName);
        rankingObject[playerNameKey] = data.playerName;
        rankingObject[scoreKey] = data.score;

        int result = 0;
        //ニフティクラウドにデータ送る
        rankingObject.SaveAsync((callBack) =>
        {
            if (callBack == null)
                result++;
            else result--;
        });
        while (result == 0)
        yield return result;
        yield return result;
    }
}
