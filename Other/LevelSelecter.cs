using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// 難易度選択クラス
/// </summary>
public class LevelSelecter : MonoBehaviour
{
    const string LevelSelectKey = "LevelSelect";
    const string LevelChangeTextBase = "むずかしさを「{0}」にした";
    [SerializeField]
    Text levelChangeText;
    [SerializeField]
    Outline[] levelUIOutline;
    Outline formerLevelChangeOutline;

    private void Awake()
    {
        LevelOutlineChange(GetLevel());
    }
    /// <summary>
    /// ボタンに貼り付けてレベル設定
    /// </summary>
    /// <param name="level"></param>
    public void SetLevelOnButton(int level)
    {
        string levelText;
        switch (level)
        {
            case 0:
                levelText = string.Format(LevelChangeTextBase, "やさしい");
                break;
            case 1:
                levelText = string.Format(LevelChangeTextBase, "ふつう");
                break;
            case 2:
                levelText = string.Format(LevelChangeTextBase, "むずかしい");
                break;
            default:
                Debug.LogError("レベルが規定値ではありません");
                levelText = "エラー";
                break;
        }
        levelChangeText.text = levelText;
        SetLevel(level);
        LevelOutlineChange(GetLevel());
    }
    void LevelOutlineChange(int level)
    {
        if (formerLevelChangeOutline != null)
            formerLevelChangeOutline.enabled = false;
        levelUIOutline[level].enabled = true;
        formerLevelChangeOutline = levelUIOutline[level];
    }
    public static void SetLevel(int level)
    {
        PlayerPrefs.SetInt(LevelSelectKey, level);
        PlayerPrefs.Save();
    }

    public static int GetLevel()
    {
        if (PlayerPrefs.HasKey(LevelSelectKey))
            return PlayerPrefs.GetInt(LevelSelectKey);
        else
        {
            PlayerPrefs.SetInt(LevelSelectKey, 0);
            PlayerPrefs.Save();
            return 0;
        }
    }
}
