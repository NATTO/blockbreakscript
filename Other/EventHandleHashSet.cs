using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// AddしたりRemoveしたりするとActionが発行されるHashset
/// </summary>
/// <typeparam name="T"></typeparam>
[System.Serializable]
public class EventHandleHashSet<T> :EventHandleCollection<HashSet<T>,T>
{    
    public EventHandleHashSet() : base()
    {
        base.list = new HashSet<T>();
    }
}
