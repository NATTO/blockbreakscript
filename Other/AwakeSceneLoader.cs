using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
/// <summary>
/// 起動時に必要な物が入ったシーンをロードするクラス
/// </summary>
public class AwakeSceneLoader : MonoBehaviour
{
    static bool isLoad = false;

    private void Awake()
    {
        if (isLoad == true) return;
        isLoad = true;
        SceneManager.LoadScene(SceneNameManager.AwakeScene, LoadSceneMode.Additive);
    }
}
