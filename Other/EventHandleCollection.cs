using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// AddしたりRemoveしたりするとActionが発行されるCollection
/// </summary>
/// <typeparam name="T"></typeparam>
/// <typeparam name="U"></typeparam>
[Serializable]
public class EventHandleCollection<T,U> where T : ICollection<U>
{
    [SerializeField]
    protected ICollection<U> list;
    List<Action<U>> addActions;
    List<Action<U>> removeActions;

    protected EventHandleCollection()
    {
        addActions = new List<Action<U>>();
        removeActions = new List<Action<U>>();
    }

    public IEnumerable<U> Value => list;

    public void AddValue(U value)
    {
        list.Add(value);
        addActions.ForEach(action => action?.Invoke(value));
    }

    public void RemoveValue(U value)
    {
        list.Remove(value);
        removeActions.ForEach(action => action?.Invoke(value));
    }

    public void AddAddAction(Action<U> value) => addActions.Add(value);

    public void AddRemoveAction(Action<U> value) => removeActions.Add(value);

    public void RemoveAddAction(Action<U> value) => addActions.Remove(value);

    public void RemoveRemoveAction(Action<U> value) => removeActions.Remove(value);
}
