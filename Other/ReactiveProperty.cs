using System;
using System.Collections.Generic;
using System.Linq;

public class ReactiveProperty<T>
{
    T value;
    public T Value
    {
        set
        {
            this.value = value;
            Notify(value);
        }
        get { return value; }
    }
    List<Action<T>> subscribeFunctions = new List<Action<T>>();

    public ReactiveProperty() { }
    public ReactiveProperty(T defaultValue) => value = defaultValue;

    public void Subscribe(Action<T> subscribeFunction)
    {
        subscribeFunctions.Add(subscribeFunction);
    }

    public void UnSubscribe(Action<T> unSubscribeFunction)
    {
        subscribeFunctions.Remove(unSubscribeFunction);
    }

    void Notify(T nextValue)
    {
        foreach(var action in subscribeFunctions)
        {
            action?.Invoke(nextValue);
        }
    }
}
