using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// ゲームシーンのロードクラス
/// </summary>
public class LoadGame : MonoBehaviour
{
    [SerializeField]
    BlockDataSettings[] blockLevels;
        

    public void LoadBlockGameScene(ProgressSettings progress)
    {
        int level = Mathf.Clamp(LevelSelecter.GetLevel(), 0, blockLevels.Length - 1);
        GameSetting.BlockDataSettings = blockLevels[level];
        GameSetting.ProgressSettings = progress;

        SceneLoader.Instance.LoadSceneAsync(SceneNameManager.BlockGameScene);
    }

    public void LoadTutorialScene()
    {
        SceneLoader.Instance.LoadSceneAsync(SceneNameManager.TutorialScene);
    }
}
