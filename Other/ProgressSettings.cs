using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// ゲームシーンの設定クラス
/// </summary>
[CreateAssetMenu()]
public class ProgressSettings : ScriptableObject
{
    /// <summary>
    /// プレイヤーを操作できる時間
    /// </summary>
    public float inGameTime;
    /// <summary>
    /// ゲームが始まるまでの時間
    /// </summary>
    public float gameStartTime;
    /// <summary>
    /// どこのランキング登録するかのデータ
    /// </summary>
    public RankingKeysData keysData;
}
