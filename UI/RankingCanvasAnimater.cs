using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
/// <summary>
/// タイトル画面のランキングオンオフするクラス
/// </summary>
public class RankingCanvasAnimater : MonoBehaviour
{
    [SerializeField]
    RectTransform targetObject;
    [SerializeField]
    Canvas rankingCanvas;
    Vector3 targetStartPosition;
    [SerializeField]
    float animationSpeedRatio = 20f;
    [SerializeField]
    float rankingFallSpeed = 1000;

    private void Awake()
    {
        targetStartPosition = targetObject.position;
    }

    public void Open()
    {
        targetObject.gameObject.SetActive(true);
    }

    public void Close()
    {
        StartCoroutine(CloseAnimation());
        IEnumerator CloseAnimation()
        {
            yield return StartCoroutine(RankingUICloseCoroutine(targetStartPosition.y, -rankingCanvas.GetComponent<RectTransform>().sizeDelta.y));
            targetObject.gameObject.SetActive(false);
        }
    }

    public IEnumerator RankingUICloseCoroutine(float startHiehgt, float endHeight)
    {
        float nowHeight = startHiehgt;
        float ratio = 0;

        while (true)
        {
            if (targetObject.localPosition.y < endHeight)
                break;

            nowHeight -= (animationSpeedRatio + ratio) * Time.deltaTime;
            
            ratio += Time.deltaTime * rankingFallSpeed;
            Vector3 nextPosition = new Vector3(targetObject.position.x, nowHeight, targetObject.position.z);
            targetObject.localPosition = nextPosition;
            yield return null;
        }


        targetObject.localPosition = targetStartPosition;
    }

}
