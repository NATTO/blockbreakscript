using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
/// <summary>
/// プレイヤーの名前を入力するクラス
/// </summary>
public class PlayerNameInputField : MonoBehaviour
{
    [SerializeField]
    GameObject inputParentObject;
    [SerializeField]
    InputField inputField;
    [SerializeField]
    Button playerNameDecideButton;

    public System.Action<string> InputCompletedAction;

    public void Initialize()
    {
        inputParentObject.gameObject.SetActive(true);
        inputField.Select();
        playerNameDecideButton.onClick.AddListener(OnInputCompleted);
    }

    void OnInputCompleted()
    {
        InputCompletedAction?.Invoke(inputField.textComponent.text);
    }
}
