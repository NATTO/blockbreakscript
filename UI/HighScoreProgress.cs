﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
/// <summary>
/// ゲームクリア時のハイスコア処理クラス
/// </summary>
public class HighScoreProgress : MonoBehaviour
{
    [SerializeField]
    GameObject highScoreUIObject;
    [SerializeField]
    GameObject rankingUIVisualizeButton;
    [SerializeField]
    GameObject rankingRegistrationUIObject;

    [SerializeField]
    Button rankingRegisterButton;
    [SerializeField]
    PlayerNameInputField highScoreNameField;
    [SerializeField]
    GameScore gameScore;
    [SerializeField]
    GameObject rankingRegisterWaitObject;
    [SerializeField]
    RankingKeysData rankingKeysData;

    ScoreData highScoreData;

    public void SetRankingKeysData(RankingKeysData data) => rankingKeysData = data;

    private void Start()
    {
        highScoreData = SaveDataManager.LoadData;
        gameScore.AddHighScoreAction(HighScore);
    }

    void HighScore(bool isHighScore)
    {
        if (isHighScore == false) { SoundManager.Instance.SEPlay(SE_ID.STAGE_CLEAR); return; }

        highScoreUIObject.SetActive(true);
        SoundManager.Instance.SEPlay(SE_ID.HIGH_SCORE);

        //ランキングに登録しない人用セーブ
        SaveDataManager.SaveScoreData(new ScoreData(rankingKeysData.scoreType,"", gameScore.Score));

        if (Application.internetReachability != NetworkReachability.NotReachable)
        {
            rankingUIVisualizeButton.SetActive(true);

            //名前決めたらランキング登録、で名前入りデータをセーブ
            rankingRegisterButton.onClick.AddListener(() =>
            {
                highScoreNameField.Initialize();
                highScoreNameField.InputCompletedAction = (string playerName) =>
                {
                    ScoreData highScoreData = new ScoreData(rankingKeysData.scoreType, playerName, gameScore.Score);
                    StartCoroutine(SetOnlineRanking(highScoreData));
                    SaveDataManager.SaveScoreData(highScoreData);
                };
            });
        }
    }

    IEnumerator SetOnlineRanking(ScoreData data)
    {
        rankingRegisterWaitObject.SetActive(true);
        IEnumerator<int> setRanking = Ranking.SetRanking(rankingKeysData.rankingClass, data);
        yield return setRanking;
        rankingRegisterWaitObject.SetActive(false);
        highScoreUIObject.SetActive(false);
        rankingRegistrationUIObject.SetActive(false);
        rankingRegisterButton.gameObject.SetActive(false);
        if (setRanking.Current == -1)
        {
            Debug.LogError("エラーです！");
        }
    }
}