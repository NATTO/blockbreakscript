using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
/// <summary>
/// リトライボタン用のクラス
/// </summary>
public class RetryButton : MonoBehaviour
{
    public void Retry()
    {
        SceneLoader.Instance.LoadSceneAsync(SceneNameManager.BlockGameScene);         
    }
}
