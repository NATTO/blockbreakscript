using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
/// <summary>
/// ランキングUIのアニメーションクラス
/// </summary>
public class RankingSelect : MonoBehaviour
{
    [SerializeField]
    RankingUse oneMinits;
    [SerializeField]
    RankingUse infinity;
    [SerializeField]
    Transform startPosition;
    [SerializeField]
    RectTransform rankingPerentTransform;
    [SerializeField]
    float positionRatio;
    [SerializeField]
    float moveTime = 1;
    Coroutine nowCoroutine;

    public enum RankingState
    {
        One_Minits = 0,
        Three_Minits = 1,
        Infinity = 2
    }

    IEnumerator RankingPositionMove(Vector3 nextPosition)
    {
        float time = 0;
        Vector3 nowPosition = rankingPerentTransform.localPosition;
        while (time <= moveTime) {

            Vector3 position = Vector3.Lerp(nowPosition, nextPosition, time);
            rankingPerentTransform.localPosition = position;
            time += Time.deltaTime;
            yield return null;
        }
        rankingPerentTransform.localPosition = nextPosition;
        nowCoroutine = null;
    }

    public void SelectView(int select)
    {
        if (nowCoroutine != null) return;
        RankingState select1 = (RankingState)select;
        switch (select1)
        {
            case RankingState.One_Minits:
                nowCoroutine = StartCoroutine(RankingPositionMove(startPosition.localPosition));
                oneMinits.LoadRanking();
                break;
            case RankingState.Infinity:
                nowCoroutine = StartCoroutine(RankingPositionMove(startPosition.localPosition + (Vector3.right * positionRatio)));
                infinity.LoadRanking();
                break;
        }
    }
}
