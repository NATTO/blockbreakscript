using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchEffect : MonoBehaviour
{
    [SerializeField]
    ParticleSystem touchDownParticle;
    [SerializeField]
    ParticleSystem touchStayParticle;

    private void Update()
    {
        var clickDown = Input.GetMouseButtonDown(0);
        var clickStay = Input.GetMouseButton(0);
        var clickUp = Input.GetMouseButtonUp(0);

        if (clickDown == true)
        {
            touchDownParticle.transform.position = Camera.main.ScreenToWorldPoint(Input.mousePosition) + Vector3.forward * 90;
            if (touchDownParticle.isPlaying)
            {
                touchDownParticle.time = 0;
            }
            touchDownParticle.Play();
        }
        if (clickStay == true)
        {
            if (touchStayParticle.isPlaying == false)
                touchStayParticle.Play();
            touchStayParticle.transform.position = Camera.main.ScreenToWorldPoint(Input.mousePosition) + Vector3.forward * 90;
        }
        if (clickUp == true)
        {
            touchStayParticle.Stop();
        }
    }
}
