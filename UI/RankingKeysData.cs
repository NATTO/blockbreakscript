using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// ランキングのデータ
/// </summary>
[CreateAssetMenu(menuName = "RankingKeys")]
public class RankingKeysData : ScriptableObject
{
    /// <summary>
    /// ニフティクラウドのクラス名
    /// </summary>
    public string rankingClass;
    /// <summary>
    /// 取得するランキングの長さ
    /// </summary>
    public int rankingLength;
    /// <summary>
    /// どの遊び方で得たスコアか
    /// </summary>
    public ScoreType scoreType;
}
