using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;
using UnityEngine.UI;

/// <summary>
/// ランキングUIクラス
/// </summary>
public class RankingView : MonoBehaviour
{
    [SerializeField]
    Text[] RankNumTexts;
    [SerializeField]
    Text[] RankingNames;
    [SerializeField]
    Text[] RankingScores;

#if UNITY_EDITOR
    public void SetRankNumTexts(Text[] nums) => RankNumTexts = nums;
    public void SetRankingNames(Text[] names) => RankingNames = names;
    public void SetRankingScores(Text[] scores) => RankingScores = scores;
#endif

    public bool DisplayRanking(List<ScoreData> scoreDatas, int length)
    {
        if (scoreDatas == null) return false;

        for (int index = 0; index < length; index++)
        {
            string num = (index + 1).ToString();
            string name;
            string score;
            if (index >= scoreDatas.Count || index < 0)
            {
                name = "ぷれいやー";
                score = "0";
            }
            else
            {
                name = scoreDatas[index].playerName;
                score = scoreDatas[index].score.ToString();
            }
            RankNumTexts[index].text = num;
            RankingNames[index].text = name;
            RankingScores[index].text = score;
        }

        return true;
    }
}