using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// アスペクト比を固定するクラス
/// </summary>
[ExecuteAlways]
public class AspectKeeper : MonoBehaviour
{
    [SerializeField]
    Camera targetCamera;

    [SerializeField]
    Vector2 aspect;

    private void Update()
    {
        var screenAspect = (float)Screen.width / (float)Screen.height;
        var targetAspect = aspect.x / aspect.y;

        var magRate = targetAspect / screenAspect;

        var viewPortRect = new Rect(0, 0, 1, 1);
        if (magRate < 1)
            viewPortRect.width = magRate;
        else
            viewPortRect.height = 1 / magRate;

        targetCamera.rect = viewPortRect;

    }
}
