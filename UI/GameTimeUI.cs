using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// ゲームの時間UI管理クラス
/// </summary>
public class GameTimeUI : MonoBehaviour
{
    const string baseText = "{0} : {1}";
    [SerializeField]
    Text timeText;
    GameProgress gameProgress;

    public void SetUp(GameProgress progress)
    {
        gameProgress = progress;
        if (this.gameProgress.InGameTime == Mathf.Infinity)
        {
            timeText.text = "INFINITY";
        }
        else
        {
            StartCoroutine(InGameTimeVisualize());
        }
    }

    private IEnumerator InGameTimeVisualize()
    {
        while (gameProgress.InGameTime > 0)
        {
            float time = gameProgress.InGameTime;
            float minits = Mathf.Floor(time / 60);
            float seconds = Mathf.Floor(time % 60);
            timeText.text = string.Format(baseText, minits, seconds);
            yield return null;
        }
        timeText.text = string.Format(baseText, 0, 0);
    }
}
