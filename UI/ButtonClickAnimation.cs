using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ButtonClickAnimation : MonoBehaviour
{
    [SerializeField]
    float clickStayShrinkSize = 0.8f;
    [SerializeField]
    float shrinkTime = 0.2f;

    Coroutine buttonClickCoroutine;
    RectTransform myTransform;

    public void SetTransform(RectTransform transform) => myTransform = transform;

    IEnumerator ButtonClickDownCoroutine()
    {
        float time = 0;
        while (time < clickStayShrinkSize)
        {
            float size = Mathf.Lerp(1, clickStayShrinkSize, time);

            myTransform.localScale = new Vector3(size, size, 1);
            time += Time.deltaTime / shrinkTime;
            yield return null;
        }
    }
    
    public void ButtonClickDown()
    {
        if (myTransform == null) myTransform = GetComponent<RectTransform>();
        buttonClickCoroutine = StartCoroutine(ButtonClickDownCoroutine());
    }

    public void ButtonClickUp()
    {
        if (buttonClickCoroutine != null)
            StopCoroutine(buttonClickCoroutine);
        buttonClickCoroutine = null;
        myTransform.localScale = Vector3.one;
    }
}
