using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// タイトルもどるボタンクラス
/// </summary>
public class ToTitleButton : MonoBehaviour
{
    public void ToTitle()
    {
        SceneLoader.Instance.LoadSceneAsync(SceneNameManager.TitleScene);
    }
}
