using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// レベル選択をした時のアニメーションクラス
/// </summary>
public class LevelSelectAnimation : MonoBehaviour
{
    const float cleannessSpeed = 0.8f;

    [SerializeField]
    CanvasGroup animationGroup;
    Coroutine nowAnimation;

    public void Animation()
    {
        if (nowAnimation != null)
        {
            StopCoroutine(nowAnimation);
            nowAnimation = null;
        }
        nowAnimation = StartCoroutine(AnimationCoroutine());
        IEnumerator AnimationCoroutine()
        {
            animationGroup.gameObject.SetActive(true);
            float alpha = 1.0f;
            while (alpha >= 0f)
            {
                alpha -= Time.deltaTime * cleannessSpeed;
                animationGroup.alpha = alpha;
                yield return null;
            }
            animationGroup.gameObject.SetActive(false);
            animationGroup.alpha = 1;
        }
    }
}
