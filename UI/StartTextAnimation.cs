using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// ゲームシーンの最初の演出クラス
/// </summary>
public class StartTextAnimation : MonoBehaviour
{
    [SerializeField]
    float moveSpeed = 0;

    public void AnimationStart()
    {
        SoundManager.Instance.SEPlay(SE_ID.GAME_START);
        StartCoroutine(Animation());
    }

    IEnumerator Animation()
    {
        WaitForFixedUpdate waitForFixedUpdate = new WaitForFixedUpdate();
        while (true)
        {
            transform.position += Vector3.left * moveSpeed;
            yield return waitForFixedUpdate;
        }
    }
}
