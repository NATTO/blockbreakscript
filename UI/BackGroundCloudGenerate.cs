using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
/// <summary>
/// 背景の雲を生成するクラス
/// </summary>
public class BackGroundCloudGenerate : MonoBehaviour
{
    const float minCloudSize = 8;
    const float maxCloudSize = 3;
    Vector2 baseSize;

    [SerializeField]
    RectTransform backGroundCanvas;
    [SerializeField]
    Image cloudImage;
    [SerializeField]
    float createSpeed;
    MonobehaviourObjectPool<Image> poolClouds;
    [SerializeField]
    Camera mainCamera;

    private void Start()
    {
        poolClouds = new MonobehaviourObjectPool<Image>(cloudImage, 10);
        baseSize = cloudImage.rectTransform.sizeDelta;
    }

    public void CloudCreateStart()
    {
        StartCoroutine(CloudCreateCoroutine());
    }

    IEnumerator CloudCreateCoroutine()
    {
        while (true)
        {
            var cloud = poolClouds.GetPoolObject();
            cloud.transform.parent = backGroundCanvas.transform;
            cloud.transform.localScale = Vector3.one;
            cloud.transform.localPosition = Vector3.zero;
            cloud.transform.localPosition = new Vector3(-backGroundCanvas.sizeDelta.x / 2,Random.Range(0,backGroundCanvas.sizeDelta.y /2),0);


            Vector2 cloudSize = baseSize;
            var sizeRatio = Random.Range(maxCloudSize, minCloudSize);
            cloudSize /= sizeRatio;
            CloudAnimation cloudAnim = cloud.GetComponent<CloudAnimation>();
            if (cloudAnim.TargetCamera == null) cloudAnim.TargetCamera = mainCamera;
            cloudAnim.MoveSpeedChange(sizeRatio);
            cloud.rectTransform.sizeDelta = cloudSize;
            yield return WaitTimeCoroutine(createSpeed);
        }
    }

    IEnumerator WaitTimeCoroutine(float timeSpeed)
    {
        float time = 0;
        while (true)
        {
            if (1 <= time) yield break;
            yield return null;
            time += Time.deltaTime * timeSpeed;
        }
    }
}
