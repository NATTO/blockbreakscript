using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// 背景の雲のアニメーションクラス
/// </summary>
public class CloudAnimation : MonoBehaviour
{
    [SerializeField]
    float moveSpeed;
    public float MoveSpeed { get => moveSpeed; }
    float runTimeMoveSpeed;
    RectTransform rectTransform;
    public Camera TargetCamera { get; set; }
    Rect rect = new Rect(0, 0, 1, 1);

    private void Start()
    {
        rectTransform = GetComponent<RectTransform>();
    }

    public void MoveSpeedChange(float ratio)
    {
        runTimeMoveSpeed = moveSpeed * ratio;
    }

    private void FixedUpdate()
    {
        rectTransform.position += Vector3.right * runTimeMoveSpeed * Time.deltaTime;

        var isVisible = rect.Contains(TargetCamera.WorldToViewportPoint(rectTransform.transform.position + (Vector3)(rectTransform.rect.min * rectTransform.lossyScale.x))) ||
           rect.Contains(TargetCamera.WorldToViewportPoint(rectTransform.transform.position + (Vector3)(rectTransform.rect.max * rectTransform.lossyScale.x)));

        if(isVisible == false)
            gameObject.SetActive(false);
    }
}
