using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NCMB;
using NCMB.Internal;
using System.Linq;
/// <summary>
/// ランキングの読み込みクラス
/// </summary>
public class RankingUse : MonoBehaviour
{
    [SerializeField]
    RankingKeysData rankingKeysData;
    [SerializeField]
    RankingView rankingView;
        
    public void LoadRanking()
    {
        StartCoroutine(LoadRankingCoroutine());
    }

    IEnumerator LoadRankingCoroutine()
    {
        var rankingData = Ranking.GetRanking(rankingKeysData.rankingClass, rankingKeysData.rankingLength);
        yield return rankingData;
        if (rankingData.Current != null)
        {
            rankingView.DisplayRanking(rankingData.Current, rankingKeysData.rankingLength);
        }
    }
}

