
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
/// <summary>
/// ブロックの生成管理クラス
/// </summary>
public class BlockGenerator : MonoBehaviour
{
    [SerializeField]
    int widthRange;
    [SerializeField]
    int heightRange;

    const int spawnHeight = 20;

    public const int blockSpan = 3;

    public static readonly Vector2 zeroPoint = new Vector2(-13.5f, 0);

    //Random.Rangeで使う変数
    int blockTypeLength = System.Enum.GetValues(typeof(Block_Type)).Cast<int>().Max() + 1;

    [SerializeField]
    BlockSpriteSettings blockSpriteSettings;
    public BlockSpriteSettings BlockSpriteSettings => blockSpriteSettings;
    [SerializeField]
    BlockDataSettings blockDataSettings;
    public BlockDataSettings BlockDataSettings => blockDataSettings;
    [SerializeField]
    BlockBase blockSample;
    [SerializeField]
    GrayBlock grayBlockSample;
    [SerializeField]
    GameObject[] blockPrefabs;
    [SerializeField]
    GameObject[] unBreakBlockPrefabs;
    [SerializeField]
    Transform blockParentObject;
    MonobehaviourObjectPool<BlockBase> blockPool;
    MonobehaviourObjectPool<GrayBlock> grayBlockPool;
    BlockManager blockManager;
    public System.Action GameOver { get; set; }
#if UNITY_EDITOR
    /// <summary>
    /// デバッグ用、壊せないブロックを作るか
    /// </summary>
    [SerializeField]
    bool isUnbreakableBlockGenerate;
#endif  

    public GameObject[] BlockPrefabs => blockPrefabs;

    [SerializeField]
    float spawnTime = 5f;

    public bool IsGenerate { get; set; } = true;
    

    public void InitializeSettings(BlockSpriteSettings spriteSettings, BlockDataSettings dataSettings,BlockManager blockManager)
    {
        blockSpriteSettings = spriteSettings;
        blockDataSettings = dataSettings;
        this.blockManager = blockManager;
    }

    public void SetUp(int widthRange, int heightRange)
    {
        this.widthRange = widthRange;
        this.heightRange = heightRange;
        blockPool = new MonobehaviourObjectPool<BlockBase>(blockSample, 20);
        grayBlockPool = new MonobehaviourObjectPool<GrayBlock>(grayBlockSample, 10);
        StartCoroutine(GenerateLoop());
    }

    public void UnbreakableBlockGenerate()
    {
#if UNITY_EDITOR
        if (isUnbreakableBlockGenerate == false) return;
#endif
        BlockBase formerGeneratedBlock = null;
        BlockBase twoAgoGenerateBlock = null;
        for (int width = 0; width < BlockManager.widthRange; width++)
        {
            for (var height = 0; height < 2; height++)
            {
                var generateBlock = GenerateUnBreakableBlock(width, height);
                if (formerGeneratedBlock == null)
                    blockManager.SetBlock(width, height, generateBlock);
                else
                {
                    //ブロックの種類を被らせないようにする
                    if (formerGeneratedBlock.BlockType == generateBlock.BlockType || twoAgoGenerateBlock?.BlockType == generateBlock.BlockType)
                    {
                        Destroy(generateBlock.gameObject);
                        height--;
                        continue;
                    }
                    else
                    {
                        blockManager.SetBlock(width, height, generateBlock);
                        generateBlock.Initialize(height, width, blockDataSettings.fallSpeed,blockManager);
                    }
                }
                twoAgoGenerateBlock = formerGeneratedBlock;
                formerGeneratedBlock = generateBlock;
            }
        }
    }

    BlockBase GenerateUnBreakableBlock(int width, int height)
    {
        GameObject obj = unBreakBlockPrefabs[Random.Range(0, unBreakBlockPrefabs.Length)];
        BlockBase block = Instantiate(obj, Vector3.zero, Quaternion.identity, blockParentObject).GetComponent<BlockBase>();
        return block;
    }

    IEnumerator GenerateLoop()
    {
        float time = 0;
        while (true)
        {
            if (IsGenerate == false) yield break;
            time += Time.deltaTime * GameProgress.InGameTimeScale;
            if (time >= spawnTime)
            {
                BlockBase generateBlock = GenerateBlock();
                if (generateBlock.Height == heightRange)
                {
                    GameOver?.Invoke();
                }
                time = 0;
            }
            yield return null;
        }
    }

    public BlockBase GenerateBlock()
    {
        Block_Type generateType = (Block_Type)Random.Range(1, blockTypeLength);

        int spwanWidth = Random.Range(0, widthRange);
        int heightCount = blockManager.GetHeightCount(spwanWidth);
        Vector3 spawnPos = new Vector3(zeroPoint.x + (spwanWidth * blockSpan), zeroPoint.y + spawnHeight, 0);

        BlockBase generateBlock;
        if (generateType == Block_Type.Gray)
        {
            GrayBlock grayBlock = grayBlockPool.GetPoolObject();
            grayBlock.oneStrikeImage = blockSpriteSettings.GrayOneStrike;
            grayBlock.twoStrikeImage = blockSpriteSettings.GrayTwoStrike;
            grayBlock.destoryCount = 3;
            generateBlock = grayBlock;
        }
        else
        {
            generateBlock = blockPool.GetPoolObject();
        }
        generateBlock.transform.parent = blockParentObject;
        generateBlock.transform.position = spawnPos;
        generateBlock.BlockSpriteRenderer().sprite = blockSpriteSettings.GetBlockSpriteFromBlockType(generateType);
        generateBlock.SetBlockType(generateType);
        generateBlock.Initialize(heightCount, spwanWidth, blockDataSettings.fallSpeed, blockManager);
        generateBlock.SetUp(heightCount * blockSpan);
        return generateBlock;
    }

}
