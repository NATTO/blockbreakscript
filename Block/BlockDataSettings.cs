using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// 難易度のデータ
/// </summary>
[CreateAssetMenu(menuName  ="BlockData/DataSettings")]
public class BlockDataSettings : ScriptableObject
{
    /// <summary>
    /// ブロックが落ちるスピード
    /// </summary>
    public float fallSpeed;
    /// <summary>
    /// ブロックの生成スピード
    /// </summary>
    public float generateSpeed;
}
