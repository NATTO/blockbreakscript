using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// 三回殴らないと壊せないブロック
/// </summary>
public class GrayBlock : BlockBase
{
    SpriteRenderer spriteRenderer;
    [SerializeField]
    public Sprite oneStrikeImage;
    [SerializeField]
    public Sprite twoStrikeImage;

    private void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    public override bool Strike()
    {
        destoryCount--;
        switch (base.destoryCount)
        {
            case 2:
                spriteRenderer.sprite = oneStrikeImage;
                break;
            case 1:
                spriteRenderer.sprite = twoStrikeImage;
                break;
        }
        if (destoryCount <= 0)
        {
            BlockEnd();
            return true;
        }
        SoundManager.Instance.SEPlay(SE_ID.BLOCK_STRIKE);
        return false;
    }
}
