using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public enum Block_Type
{
    Blank = 0,
    Red,
    Yellow,
    Green,
    Gray
}
/// <summary>
/// ブロックの基底クラス
/// </summary>
public class BlockBase : MonoBehaviour
{
    const int spriteSize = 29;
    bool isFall = true;
    public bool IsFall => isFall;
    SpriteRenderer blockRenderer;

    public SpriteRenderer BlockSpriteRenderer()
    {
        if (blockRenderer == null)
            blockRenderer = GetComponent<SpriteRenderer>();
        return blockRenderer;
    }

    public int destoryCount = 1;

    float fallSpeed = 0.3f;

    float fallEndHeight = 0;
    public void SetFallEndHeight(float fallEndHeight) => this.fallEndHeight = fallEndHeight;

    float sideMoveSpeed = 3f;
    [SerializeField]
    int height = 0;

    int width = 0;

    [SerializeField]
    Block_Type blockType;

    [SerializeField]
    GameObject[] debriObjects;

    public void SetBlockType(Block_Type type)
    {
        blockType = type;
    }

    [SerializeField, Tooltip("trueなら殴りだと壊れない")]
    bool isUnbreak = false;

    public bool IsUnBreak => isUnbreak;

    public Block_Type BlockType => blockType;

    public int Height => height;

    public int Width => width;

    Coroutine fallCoroutine;
    BlockManager blockManager;

    public void Initialize(int height, int width, float fallSpeed,BlockManager blockManager)
    {
        this.width = width;
        this.height = height;
        this.fallSpeed = fallSpeed;
        this.blockManager = blockManager;
    }


    /// <summary>
    /// 落下到達地点渡してそこまで落とす
    /// </summary>
    /// <param name="fallEndHeight">ブロックが落ちるy座標</param>
    public void SetUp(float fallEndHeight)
    {
        this.fallEndHeight = fallEndHeight;
        BlockSpriteRenderer().enabled = true;
        if (blockManager.fallBlockList.Value.ToList().FindAll(block => block.Width == width)
            .Find(sameWidthBlock => sameWidthBlock.Height == height) != null)
        {
            height++;
            this.fallEndHeight = height * BlockGenerator.blockSpan;
        }
        StartFall();
    }

    public void StartFall()
    {
        if (fallCoroutine != null)
        {
            StopCoroutine(fallCoroutine);
            fallCoroutine = null;
        }
        fallCoroutine = StartCoroutine(Fall());
    }

    public void UpdateFallEndHeight(int height)
    {
        if (blockManager.BlockList[width][this.height] != null)
        {
            if (blockManager.BlockList[width][this.height] == this)
            {
                blockManager.SetBlock(width, this.height, null);
            }
        }
        this.fallEndHeight = height * BlockGenerator.blockSpan;
        this.height = height;
        if (!isFall)
        {
            StartFall();
        }
    }

    /// <summary>
    /// 移動（横）
    /// </summary>
    /// <param name="moveValue">移動量</param>
    public IEnumerator SideMove(int moveValue)
    {
        SoundManager.Instance.SEPlay(SE_ID.BLOCK_MOVE);
        float time = 0;
        Vector2 startPosition = transform.position;
        Vector2 toPosition = new Vector2(BlockGenerator.zeroPoint.x + (BlockGenerator.blockSpan * (width + moveValue)), BlockGenerator.zeroPoint.y + (BlockGenerator.blockSpan * height));
        blockManager?.SetBlock(width, Height, null);
        while (time <= 1)
        {
            transform.position = Vector2.Lerp(startPosition, toPosition, time);
            yield return null;
            time += Time.deltaTime * GameProgress.InGameTimeScale * sideMoveSpeed;
        }
        int nextWidth = width + moveValue;
        if (nextWidth <= BlockManager.widthRange)
            blockManager?.SetBlock(nextWidth, Height, this);

        width += moveValue;

        blockManager?.AllFall();
        blockManager?.FalledBlock(this);
    }

    WaitForFixedUpdate waitFixedUpdate = new WaitForFixedUpdate();
    /// <summary>
    /// 指定の場所まで落ちる
    /// </summary>
    /// <returns></returns>
    IEnumerator Fall()
    {
        isFall = true;
        blockManager?.fallBlockList.AddValue(this);
        Vector3 fallValue = new Vector3(0, fallSpeed, 0);
        

        while (IsFallEnd())
        {
            //落ちる先にブロックがあったらその上に行く
            if (blockManager?.GetBlock(Width, Height) != null)
                UpdateFallEndHeight(Height + 1);

            transform.position -= fallValue * Time.timeScale * GameProgress.InGameTimeScale;
            yield return waitFixedUpdate;
        }

        transform.position = new Vector3(transform.position.x, fallEndHeight, transform.position.z);
        isFall = false;
        if (height == BlockManager.heightRange) yield break;
        if (blockManager?.GetBlock(width, Height) == null)
            blockManager?.SetBlock(width, height, this);
        else
        {
            for (int i = 1; i + height < BlockManager.heightRange; i++)
            {
                if (blockManager?.GetBlock(Width, height + i) == null)
                {
                    blockManager?.SetBlock(Width, height + i, this);
                    break;
                }
            }
        }
        SoundManager.Instance.SEPlay(SE_ID.BLOCK_FALLEND);
        //BlockManagerに落ちたことを通知
        blockManager?.FalledBlock(this);
    }

    bool IsFallEnd()
    {
        float nowHeight = transform.position.y;
        return nowHeight >= fallEndHeight;
    }

    /// <summary>
    /// ダメージ与えられる処理
    /// </summary>
    public virtual bool Strike()
    {
        destoryCount--;
        if (destoryCount <= 0)
        {
            BlockEnd();
            return true;
        }
        SoundManager.Instance.SEPlay(SE_ID.BLOCK_STRIKE);
        return false;
    }

    protected void BlockEnd()
    {
        blockManager?.SetBlock(width, height, null);
        blockManager?.AllFall();
        SoundManager.Instance.SEPlay(SE_ID.BLOCK_DESTROY);
        StartCoroutine(DestroyAnimation());
    }

    #region animation
    public IEnumerator DestroyAnimation()
    {
        SpriteRenderer spriteRenderer = GetComponent<SpriteRenderer>();
        spriteRenderer.enabled = false;
        Sprite sprite = spriteRenderer.sprite;
        Texture2D texture = sprite.texture;
        
        int forceX = -1;
        int forceY = 1;
        for (int i = 0; i < debriObjects.Length; i++)
        {
            debriObjects[i].SetActive(true);

            SpriteRenderer renderer = debriObjects[i].GetComponent<SpriteRenderer>();
            Rigidbody rb = debriObjects[i].GetComponent<Rigidbody>();
            rb.useGravity = false;
            renderer.color = spriteRenderer.color;
            renderer.sprite = Sprite.Create(texture, new Rect(Random.Range(0, texture.width - 30), Random.Range(0, texture.height - 30), spriteSize, spriteSize), Vector2.one / 2);
            renderer.drawMode = SpriteDrawMode.Sliced;
            renderer.size = Vector2.one;
            debriObjects[i].transform.position = transform.position;
            rb.AddForce(new Vector3(40 * forceX, 40 * forceY, 0), ForceMode.Impulse);

            if (forceX < 0)
                forceX += 2;
            else forceX -= 2;
            if (i % 2 == 0)
                if (forceY < 0)
                    forceY += 2;
                else forceY -= 2;
        }
        float progress = 0;

        while (progress <= 0.25f)
        {
            yield return null;
            progress += Time.deltaTime;
        }

        foreach (var debriObject in debriObjects)
            debriObject.SetActive(false);
        gameObject.SetActive(false);
    }
    #endregion
}
