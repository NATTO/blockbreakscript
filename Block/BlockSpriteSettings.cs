using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// ブロックのスプライト
/// </summary>
[CreateAssetMenu(menuName = "BlockData/SpriteSetting")]
public class BlockSpriteSettings : ScriptableObject
{
    public Sprite Red;
    public Sprite Yellow;
    public Sprite Green;
    public Sprite Gray;
    public Sprite GrayOneStrike;
    public Sprite GrayTwoStrike;
    public Sprite GetBlockSpriteFromBlockType(Block_Type type)
    {
        switch (type)
        {
            case Block_Type.Red: return Red;
            case Block_Type.Yellow: return Yellow;
            case Block_Type.Green: return Green;
            case Block_Type.Gray: return Gray;
            default: return null;
        }
    }
}
