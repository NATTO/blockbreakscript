using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class TutorialProgress : MonoBehaviour
{
    [SerializeField]
    Text scoreText;
    [SerializeField]
    Text tutorialText;
    [SerializeField]
    ScriptableTutorialText moveTutorialText;
    [SerializeField]
    ScriptableTutorialText pushTutorialText;
    [SerializeField]
    ScriptableTutorialText strikeTutorialText;
    [SerializeField]
    ScriptableTutorialText pointGetTutorialText;
    [SerializeField]
    ScriptableTutorialText gameOverTutorialText;
    [SerializeField]
    ScriptableTutorialText tutorialEndText;

    [SerializeField]
    Image upImage;
    [SerializeField]
    Image downImage;
    [SerializeField]
    Image leftImage;
    [SerializeField]
    Image rightImage;
    [SerializeField]
    Image strikeImage;
    [SerializeField]
    Image pushImage;
    [SerializeField]
    ButtonClickAnimation clickAnimation;

    [SerializeField]
    BlockBase normalBlock;


    [SerializeField]
    PlayerMove playerMove;
    [SerializeField]
    PlayerDeath playerDeath;
    [SerializeField]
    SpriteRenderer spriteRenderer;
    [SerializeField]
    Sprite idleSprite;
    [SerializeField]
    Sprite moveXSprite;
    [SerializeField]
    Sprite moveUpSprite;
    [SerializeField]
    Sprite moveDownSprite;
    [SerializeField]
    Sprite pushSprite;
    [SerializeField]
    Sprite strikeWaitSprite;

    [SerializeField]
    float textSpeed;

    [SerializeField]
    float tutorialWaitTime;

    [SerializeField]
    float blockBreakAnimationTime;

    [SerializeField]
    float blockBreakFlashingTime;

    [SerializeField]
    Vector3 blockInitializePosition;

    [SerializeField]
    Vector3 playerDeathBlockInitializePosition;

    WaitForSeconds waitForSeconds;

    IEnumerator Start()
    {
        SoundManager.Instance.BGMPlay(BGM_ID.TUTORIAL);
        waitForSeconds = new WaitForSeconds(tutorialWaitTime);
        yield return waitForSeconds;
        yield return MoveTutorialCoroutine();
        yield return waitForSeconds;
        yield return PushTutorialCoroutine();
        yield return waitForSeconds;
        yield return StrikeTutorialCoroutine();
        spriteRenderer.sprite = idleSprite;
        yield return waitForSeconds;
        yield return PointGetTutorialCoroutine();
        yield return waitForSeconds;
        yield return GameOverTutorialCoroutine();
        yield return waitForSeconds;
        yield return TutorialEndCoroutine();
        yield return waitForSeconds;
        SoundManager.Instance.BGMFadeOut();
        SceneLoader.Instance.LoadSceneAsync(SceneNameManager.TitleScene);
    }

    IEnumerator MoveTutorialCoroutine()
    {
        TextReset(tutorialText);
        StartCoroutine(DrawText(tutorialText, moveTutorialText.text));
        clickAnimation.SetTransform(rightImage.rectTransform);
        clickAnimation.ButtonClickDown();
        spriteRenderer.sprite = moveXSprite;
        yield return playerMove.MoveXCoroutine(1);
        clickAnimation.ButtonClickUp();
        clickAnimation.SetTransform(leftImage.rectTransform);
        clickAnimation.ButtonClickDown();
        spriteRenderer.flipX = true;
        yield return playerMove.MoveXCoroutine(-1);
        clickAnimation.ButtonClickUp();
        clickAnimation.SetTransform(upImage.rectTransform);
        clickAnimation.ButtonClickDown();
        spriteRenderer.flipX = false;
        spriteRenderer.sprite = moveUpSprite;
        yield return playerMove.MoveYCoroutine(1);
        clickAnimation.ButtonClickUp();
        clickAnimation.SetTransform(downImage.rectTransform);
        clickAnimation.ButtonClickDown();
        spriteRenderer.sprite = moveDownSprite;
        yield return playerMove.MoveYCoroutine(-1);
        clickAnimation.ButtonClickUp();
        spriteRenderer.sprite = idleSprite;
    }

    IEnumerator BlockFallCoroutine(BlockBase block, int height, bool isEndWait = true)
    {
        block.SetFallEndHeight(height);
        block.StartFall();
        while (block.IsFall == true)
        {
            yield return null;
        }
        if (isEndWait)
            yield return waitForSeconds;
    }

    IEnumerator PushTutorialCoroutine()
    {
        TextReset(tutorialText);
        StartCoroutine(DrawText(tutorialText, pushTutorialText.text));
        var block = Instantiate(normalBlock, blockInitializePosition, Quaternion.identity);
        yield return BlockFallCoroutine(block, 0);
        clickAnimation.SetTransform(pushImage.rectTransform);
        clickAnimation.ButtonClickDown();
        spriteRenderer.sprite = pushSprite;
        yield return block.SideMove(3);
        clickAnimation.ButtonClickUp();
        spriteRenderer.sprite = idleSprite;
        yield return BlockFallCoroutine(block, -10);
    }

    IEnumerator StrikeTutorialCoroutine()
    {
        TextReset(tutorialText);
        StartCoroutine(DrawText(tutorialText, strikeTutorialText.text));
        var block = Instantiate(normalBlock, blockInitializePosition, Quaternion.identity);
        yield return BlockFallCoroutine(block, 0);
        clickAnimation.SetTransform(strikeImage.rectTransform);
        clickAnimation.ButtonClickDown();
        spriteRenderer.sprite = strikeWaitSprite;
        yield return new WaitForSeconds(0.5f);
        spriteRenderer.sprite = pushSprite;
        block.Strike();
        clickAnimation.ButtonClickUp();
        yield return new WaitForSeconds(0.5f);
    }

    IEnumerator PointGetTutorialCoroutine()
    {
        TextReset(tutorialText);
        StartCoroutine(DrawText(tutorialText, pointGetTutorialText.text));
        var block = Instantiate(normalBlock, blockInitializePosition, Quaternion.identity);
        var block2 = Instantiate(normalBlock, blockInitializePosition, Quaternion.identity);
        var block3 = Instantiate(normalBlock, blockInitializePosition, Quaternion.identity);
        List<BlockBase> blocks = new List<BlockBase>(3)
        {
            block,
            block2,
            block3
        };
        yield return BlockFallCoroutine(block, 0);
        yield return BlockFallCoroutine(block2, 3);
        yield return BlockFallCoroutine(block3, 6, false);
        yield return BlockBreakCoroutine(blocks);
        scoreText.text = GameScore.scoreTextBase + GameScore.baseScore * 3;
    }

    IEnumerator BlockBreakCoroutine(List<BlockBase> breakBlocks)
    {
        float animProgressTime = 0;
        float flashTime = 0;
        List<SpriteRenderer> breakBlocksList = breakBlocks.ConvertAll(block => block.GetComponent<SpriteRenderer>()).ToList();

        while (animProgressTime < blockBreakAnimationTime)
        {
            if (breakBlocksList.Any(block => block == null)) break;
            if (flashTime > blockBreakFlashingTime)
            {
                breakBlocksList.ForEach(block => block.enabled = !block.enabled);
                flashTime = 0;
            }
            yield return null;
            animProgressTime += Time.deltaTime;
            flashTime += Time.deltaTime;
        }

        breakBlocksList.ForEach(block => block.enabled = false);
    }

    IEnumerator GameOverTutorialCoroutine()
    {
        TextReset(tutorialText);
        StartCoroutine(DrawText(tutorialText, gameOverTutorialText.text));
        var block = Instantiate(normalBlock, blockInitializePosition, Quaternion.identity);
        var block2 = Instantiate(normalBlock, blockInitializePosition, Quaternion.identity);
        var block3 = Instantiate(normalBlock, blockInitializePosition, Quaternion.identity);
        var block4 = Instantiate(normalBlock, blockInitializePosition, Quaternion.identity);
        var block5 = Instantiate(normalBlock, playerDeathBlockInitializePosition, Quaternion.identity);
        yield return BlockFallCoroutine(block, 0);
        yield return BlockFallCoroutine(block2, 3);
        yield return BlockFallCoroutine(block3, 6);
        yield return BlockFallCoroutine(block4, 9);
        yield return BlockFallCoroutine(block5, 0, false);
        playerDeath.Death();
    }

    IEnumerator TutorialEndCoroutine()
    {
        TextReset(tutorialText);
        yield return DrawText(tutorialText, tutorialEndText.text);
    }

    void TextReset(Text text)
    {
        text.text = string.Empty;
    }

    IEnumerator DrawText(Text textUI, string text)
    {
        int textLength = 0;
        float time = 0;
        while (textLength != text.Length)
        {
            time += Time.deltaTime * textSpeed;
            if (time > 1)
            {
                textLength++;
                time = 0;
            }
            textUI.text = text.Substring(0, textLength);
            yield return null;
        }
        textUI.text = text;
    }
}
