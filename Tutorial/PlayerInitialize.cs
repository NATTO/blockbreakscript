using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInitialize : MonoBehaviour
{
    [SerializeField]
    PlayerInputManager playerInputManager;
    [SerializeField]
    PlayerSpriteManager playerSpriteManager;
    [SerializeField]
    PlayerStateManager playerStateManager;
    [SerializeField]
    PlayerMove playerMove;
    [SerializeField]
    PlayerPush playerPush;
    [SerializeField]    
    PlayerData playerData;


            
    private void Awake()
    {
        playerStateManager.Initialize(null);
        playerMove.Initialize(playerData);
        playerSpriteManager.Initialize();
        playerInputManager.Initialize();
        playerPush.Initialize();

        transform.position = playerData.GetPlayerPositionInWorld();
    }
}
