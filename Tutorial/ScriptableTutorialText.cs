using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "TutorialText")]
public class ScriptableTutorialText : ScriptableObject
{
    [Multiline(10)]
    public string text;
}
