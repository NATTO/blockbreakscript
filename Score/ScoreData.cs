using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// セーブデータ用のスコアデータ
/// </summary>
[Serializable]
public class ScoreData
{
    public ScoreType scoreType;
    public string playerName;
    public uint score;

    public ScoreData(string jsonData)
    {
        var loadData = JsonUtility.FromJson<ScoreData>(jsonData);
        playerName = loadData.playerName;
        score = loadData.score;
    }

    public ScoreData()
    {

    }

    public ScoreData(string playerName, uint score)
    {
        this.playerName = playerName;
        this.score = score;
    }

    public ScoreData(ScoreType scoreType, string playerName, uint score)
    {
        this.scoreType = scoreType;
        this.playerName = playerName;
        this.score = score;
    }

    public string GetJsonData()
    {
        return JsonUtility.ToJson(this);
    }
}

public enum ScoreType
{
    OneMinits,
    Infinity
}