using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// コンボの管理クラス
/// </summary>
public class ComboCounter
{
    int comboCount = 1;
    public int ComboCount => comboCount;

    public ComboCounter(BlockManager blockManager)
    {
        blockManager.AddBlockBreakAction(CountPlas);
        blockManager.AddFalledBlockAction(x => { if (x == false) comboCount = 1; });
    }

    void CountPlas(int value)
    {
        comboCount++;
    }
}
