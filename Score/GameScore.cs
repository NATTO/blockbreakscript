using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
/// <summary>
/// スコアの管理クラス
/// </summary>
public class GameScore : MonoBehaviour
{
    public const uint blockBreakScore = 100;
    public const uint grayBlockBreakScore = blockBreakScore * 3;
    public const uint baseScore = 1000;
    public const string scoreTextBase = "すこあ:";
    const string comboText = "×{0}　ぼーなす！";

    [SerializeField]
    Text scoreTextUI;
    [SerializeField]
    Text gameEndScoreText;
    [SerializeField]
    Text ComboCountText;
    ComboCounter counter;
    uint score = 0;
    public uint Score => score;

    GameProgress progress;
    List<Action<bool>> highScoreAction = new List<Action<bool>>();
    ScoreType scoreType;

    /// <summary>
    /// ハイスコアの時のアクションを追加、trueならハイスコア、falseならハイスコアではない
    /// </summary>
    /// <param name="highScoreAction"></param>
    public void AddHighScoreAction(Action<bool> highScoreAction) => this.highScoreAction.Add(highScoreAction);

    public void Initialize(ScoreType scoreType, BlockManager blockManager, GameProgress gameProgress)
    {
        this.scoreType = scoreType;
        this.progress = gameProgress;
        blockManager.AddBlockBreakAction(BlockBreak);
        blockManager.AddFalledBlockAction(FalledBlock);
        counter = new ComboCounter(blockManager);
        progress.AddGameEndAction(HighScoreProcess);
        progress.AddGameEndAction(GameEndScoreText);
    }

    void FalledBlock(bool isBreak)
    {
        if (isBreak && counter.ComboCount > 1)
            ComboCountText.text = string.Format(comboText, counter.ComboCount);
        else
            ComboCountText.text = "";
    }

    public void AllErase()
    {
        IncreaseScore((uint)(((BlockManager.widthRange + BlockManager.heightRange) * baseScore) * counter.ComboCount));
    }

    public void IncreaseScore(uint score)
    {
        this.score += score;
        ScoreTextDisplay(this.score);
    }

    void HighScoreProcess()
    {
        ScoreData saveDataScore = SaveDataManager.LoadScoreData(scoreType);

        if (saveDataScore == null)
            highScoreActionInvoke(0, score);
        else
            highScoreActionInvoke(saveDataScore.score, score);


        void highScoreActionInvoke(uint savedataHighScore, uint nowScore)
        {
            highScoreAction.ForEach(action => action?.Invoke(nowScore > savedataHighScore ? true : false));
        }
    }

    void GameEndScoreText() => gameEndScoreText.text = scoreTextBase + score;

    void BlockBreak(int breakCount)
    {
        if (progress.ProgressState.Value != GameProgress.Game_Progress_State.End)
            score += (uint)(baseScore * breakCount) * (uint)counter.ComboCount;
        ScoreTextDisplay(score);
    }

    void ScoreTextDisplay(uint score)
    {
        scoreTextUI.text = scoreTextBase + score;
    }
}
